﻿Lp.	Tytuł uchwały	Numer druku	Data uchw.	Ogłoszono
1	w sprawie wyboru Marszałka Sejmu		20-10-1997	Tekst uchwały
		Przebieg procesu legislacyjnegoGP	pos. nr 1	M.P. nr 79, poz. 762
2	w sprawie ustalenia liczby wicemarszałków Sejmu	Przebieg procesu legislacyjnego1	20-10-1997	Tekst uchwały
		20-10-1997	pos. nr 1	M.P. nr 80, poz. 776
		/GP		
3	w sprawie wyboru wicemarszałków Sejmu		20-10-1997	Tekst uchwały
		Przebieg procesu legislacyjnegoGP	pos. nr 1	M.P. nr 80, poz. 777
4	w sprawie wyboru Sekretarzy Sejmu	Przebieg procesu legislacyjnego2	20-10-1997	Tekst uchwały
		20-10-1997	pos. nr 1	M.P. nr 80, poz. 778
		/PS		
5	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektu uchwały Sejmu Rzeczypospolitej Polskiej w sprawie zmiany Regulaminu Sejmu	Przebieg procesu legislacyjnego4	21-10-1997	Tekst uchwały
		20-10-1997	pos. nr 1	Niepublikowana
		/GP		
6	w sprawie zmiany Regulaminu Sejmu Rzeczypospolitej Polskiej	Przebieg procesu legislacyjnego3	28-10-1997	Tekst uchwały
		20-10-1997	pos. nr 1	M.P. nr 80, poz. 779
		/GP		
7	w sprawie wyboru sędziów Trybunału Konstytucyjnego	Przebieg procesu legislacyjnego11	05-11-1997	Tekst uchwały
		28-10-1997	pos. nr 1	M.P. nr 82, poz. 789
		/GP		
8	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia pilnych rządowych projektów ustaw	Przebieg procesu legislacyjnego17	05-11-1997	Tekst uchwały
		04-11-1997	pos. nr 1	Niepublikowana
		/PS		
9	w sprawie wyboru Trybunału Stanu	Przebieg procesu legislacyjnego5	05-11-1997	Tekst uchwały
		23-10-1997	pos. nr 1	M.P. nr 82, poz. 790
		/PS		
10	w sprawie wyboru Trybunału Stanu	Przebieg procesu legislacyjnego21	06-11-1997	Tekst uchwały
		06-11-1997	pos. nr 1	M.P. nr 82, poz. 791
		/GP		
11	w sprawie wyboru składu osobowego stałych komisji sejmowych	Przebieg procesu legislacyjnego22	10-11-1997	Tekst uchwały
		07-11-1997	pos. nr 2	Niepublikowana
		/PS		
12	z okazji Święta 11 Listopada	Przebieg procesu legislacyjnego23	11-11-1997	Tekst uchwały
		07-11-1997	pos. nr 2	M.P. nr 82, poz. 793
		/GP		
13	w sprawie wotum zaufania dla Rady Ministrów		11-11-1997	Tekst uchwały
		Przebieg procesu legislacyjnegoRM	pos. nr 2	M.P. nr 82, poz. 792
14	w sprawie wyboru sędziów Trybunału Konstytucyjnego	Przebieg procesu legislacyjnego12	19-11-1997	Tekst uchwały
		30-10-1997	pos. nr 3	M.P. nr 83, poz. 810
		/GP		
15	w sprawie wyboru posłów członków Krajowej Rady Sądownictwa	Przebieg procesu legislacyjnego24	19-11-1997	Tekst uchwały
		07-11-1997	pos. nr 3	M.P. nr 83, poz. 809
		/GP		
16	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego39	19-11-1997	Tekst uchwały
		18-11-1997	pos. nr 3	Niepublikowana
		/PS		
17	w sprawie wyboru składu osobowego Komisji Etyki Poselskiej	Przebieg procesu legislacyjnego26	20-11-1997	Tekst uchwały
		20-11-1997	pos. nr 3	Niepublikowana
		/PS		
18	w sprawie ustalenia liczby członków Komisji do Spraw Służb Specjalnych oraz trybu zgłaszania kandydatów na członków Komisji	Przebieg procesu legislacyjnego40	20-11-1997	Tekst uchwały
		18-11-1997	pos. nr 3	Niepublikowana
		/PS		
19	w sprawie trybu zgłaszania kandydatów na stanowisko Generalnego Inspektora Ochrony Danych Osobowych	Przebieg procesu legislacyjnego78	04-12-1997	Tekst uchwały
		02-12-1997	pos. nr 4	Niepublikowana
		/PS		
20	w sprawie wyboru oskarżycieli przed Trybunałem Stanu	Przebieg procesu legislacyjnego84	04-12-1997	Tekst uchwały
		02-12-1997	pos. nr 4	Niepublikowana
		/ODK		
21	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego88	04-12-1997	Tekst uchwały
		04-12-1997	pos. nr 4	Niepublikowana
		/PS		
22	w sprawie utworzenia Polskiej Grupy Unii Międzyparlamentarnej	Przebieg procesu legislacyjnego89	12-12-1997	Tekst uchwały
		04-12-1997	pos. nr 5	Niepublikowana
		/PS		
23	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego125	17-12-1997	Tekst uchwały
		17-12-1997	pos. nr 6	Niepublikowana
		/PS		
24	w sprawie wyboru Komisji do Spraw Służb Specjalnych	Przebieg procesu legislacyjnego127	18-12-1997	Tekst uchwały
		17-12-1997	pos. nr 6	Niepublikowana
		/PS		
25	w sprawie powołania członków Rady Polityki Pieniężnej	Przebieg procesu legislacyjnego143	08-01-1998	Tekst uchwały
		23-12-1997	pos. nr 8	M.P. nr 1, poz. 1
		/GP		
26	w sprawie powołania członka Rady Polityki Pieniężnej	Przebieg procesu legislacyjnego167	06-02-1998	Tekst uchwały
		15-01-1998	pos. nr 11	M.P. nr 5, poz. 52
		/GP		
27	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do zmian w kodyfikacjach dotyczących wykroczeń	Przebieg procesu legislacyjnego189	06-02-1998	Tekst uchwały
		05-02-1998	pos. nr 11	Niepublikowana
		/PS		
28	w sprawie powołania Prezesa Narodowego Banku Polskiego	Przebieg procesu legislacyjnego200	19-02-1998	Tekst uchwały
		17-02-1998	pos. nr 12	M.P. nr 6, poz. 95
		/Prezydent		
29	Rezolucja w sprawie przedstawienia Sejmowi Rzeczypospolitej Polskiej sprawozdania z prowadzonych działań osłonowych podjętych w związku z uwolnieniem cen nośników energii	Przebieg procesu legislacyjnego216	05-03-1998	Tekst uchwały
		03-03-1998	pos. nr 13	Niepublikowana
		/PBM		
30	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego219	05-03-1998	Tekst uchwały
		04-03-1998	pos. nr 13	Niepublikowana
		/PS		
31	w sprawie członkostwa Polski w Unii Europejskiej	Przebieg procesu legislacyjnego223	20-03-1998	Tekst uchwały
		10-03-1998	pos. nr 14	M.P. nr 9, poz. 174
		/INE		
32	Oświadczenie w związku z trzydziestą rocznicą wydarzeń marcowych	Przebieg procesu legislacyjnego240	20-03-1998	Tekst uchwały
		19-03-1998	pos. nr 14	M.P. nr 9, poz. 175
		/PS		
33	w sprawie powołania Generalnego Inspektora Ochrony Danych Osobowych	Przebieg procesu legislacyjnego251	02-04-1998	Tekst uchwały
		27-03-1998	pos. nr 15	M.P. nr 11, poz. 188
		/GP		
34	w sprawie trybu prac nad reformą administracji publicznej i zasad jej wdrażania	Przebieg procesu legislacyjnego191	03-04-1998	Tekst uchwały
		14-01-1998	pos. nr 15	M.P. nr 10, poz. 182
		/GP		
35	w sprawie zmiany Regulaminu Sejmu Rzeczypospolitej Polskiej	Przebieg procesu legislacyjnego245	03-04-1998	Tekst uchwały
		20-03-1998	pos. nr 15	M.P. nr 10, poz. 181
		/PS		
36	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących powszechnego dostępu do archiwów i dokumentacji byłych organów bezpieczeństwa państwa	Przebieg procesu legislacyjnego290	23-04-1998	Tekst uchwały
		22-04-1998	pos. nr 16	Niepublikowana
		/PS		
37	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego291	23-04-1998	Tekst uchwały
		22-04-1998	pos. nr 16	Niepublikowana
		/PS		
38	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia rządowych projektów ustaw o zmianie niektórych ustaw określających kompetencje organów administracji publicznej	Przebieg procesu legislacyjnego307	24-04-1998	Tekst uchwały
		24-04-1998	pos. nr 16	Niepublikowana
		/PS		
39	w sprawie przeciwdziałania i zwalczania zjawisk patologicznych wśród nieletnich	Przebieg procesu legislacyjnego133	07-05-1998	Tekst uchwały
		12-12-1997	pos. nr 17	M.P. nr 14, poz. 207
		/GP		
40	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do spraw zmian w kodyfikacjach	Przebieg procesu legislacyjnego323	08-05-1998	Tekst uchwały
		06-05-1998	pos. nr 17	Niepublikowana
		/PS		
41	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących ustroju oraz samorządu miasta stołecznego Warszawy	Przebieg procesu legislacyjnego363	22-05-1998	Tekst uchwały
		22-05-1998	pos. nr 18	Niepublikowana
		/PS		
42	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących ubezpieczeń społecznych	Przebieg procesu legislacyjnego395	04-06-1998	Tekst uchwały
		03-06-1998	pos. nr 20	Niepublikowana
		/PS		
43	w sprawie prób nuklearnych prowadzonych przez Republikę Indii oraz Islamską Republikę Pakistanu	Przebieg procesu legislacyjnego387	05-06-1998	Tekst uchwały
		02-06-1998	pos. nr 20	M.P. nr 18, poz. 261
		/GP		
44	w sprawie potępienia totalitaryzmu komunistycznego	Przebieg procesu legislacyjnego192	18-06-1998	Tekst uchwały
		04-02-1998	pos. nr 21	M.P. nr 20, poz. 287
		/GP		
		Przebieg procesu legislacyjnego206		
		20-02-1998		
		/GP		
45	w sprawie wyboru posła członka Krajowej Rady Sądownictwa	Przebieg procesu legislacyjnego356	18-06-1998	Tekst uchwały
		03-06-1998	pos. nr 21	M.P. nr 20, poz. 288
		/GP		
46	o zmianie uchwały w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia rządowych projektów ustaw o zmianie niektórych ustaw określających kompetencje organów administracji publicznej	Przebieg procesu legislacyjnego425	18-06-1998	Tekst uchwały
		17-06-1998	pos. nr 21	Niepublikowana
		/PS		
47	"Oświadczenie w sprawie rezolucji Bundestagu ""Wypędzeni, przesiedleńcy i mniejszości niemieckie są pomostem między Niemcami i ich wschodnimi sąsiadami"" z dnia 29 maja 1998 r."	Przebieg procesu legislacyjnego456	03-07-1998	Tekst uchwały
		30-06-1998	pos. nr 23	M.P. nr 23, poz. 332
		/SZA		
48	sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących spółdzielczości mieszkaniowej	Przebieg procesu legislacyjnego496	16-07-1998	Tekst uchwały
		15-07-1998	pos. nr 24	Niepublikowana
		/PS		
49	Zasady Etyki Poselskiej	Przebieg procesu legislacyjnego484	17-07-1998	Tekst uchwały
		10-07-1998	pos. nr 24	M.P. nr 24, poz. 338
		/EPS		
50	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego497	17-07-1998	Tekst uchwały
		16-07-1998	pos. nr 24	Niepublikowana
		/PS		
51	w sprawie przyjęcia sprawozdania z działalności Najwyższej Izby Kontroli	Przebieg procesu legislacyjnego483	23-07-1998	Tekst uchwały
		10-07-1998	pos. nr 25	M.P. nr 25, poz. 363
		/FPB		
52	w sprawie przyjęcia sprawozdania z wykonania budżetu państwa za okres od dnia 1 stycznia do dnia 31 grudnia 1997 r. oraz w sprawie absolutorium dla Rady Ministrów	Przebieg procesu legislacyjnego488	23-07-1998	Tekst uchwały
		14-07-1998	pos. nr 25	M.P. nr 25, poz. 362
		/FPB		
53	w sprawie zawieszenia stosowania art. 56s ust. 4 Regulaminu Sejmu w odniesieniu do niektórych projektów ustaw	Przebieg procesu legislacyjnego525	23-07-1998	Tekst uchwały
		21-07-1998	pos. nr 25	Niepublikowana
		/PS		
54	w sprawie sprawozdania Krajowej Rady Radiofonii i Telewizji z rocznej działalności	Przebieg procesu legislacyjnego304	24-07-1998	Tekst uchwały
		22-04-1998	pos. nr 25	M.P. nr 25, poz. 365
		/KSP		
55	w sprawie organizacji XX Zimowych Igrzysk Olimpijskich w 2006 r. w Zakopanem	Przebieg procesu legislacyjnego537	24-07-1998	Tekst uchwały
		22-07-1998	pos. nr 25	M.P. nr 25, poz. 364
		/KFT		
56	w sprawie systemu alimentacyjnego	Przebieg procesu legislacyjnego272	10-09-1998	Tekst uchwały
		03-04-1998	pos. nr 27	M.P. nr 30, poz. 413
		/ROD		
57	w sprawie ustanowienia Dnia Polskiego Państwa Podziemnego	Przebieg procesu legislacyjnego556	11-09-1998	Tekst uchwały
		25-07-1998	pos. nr 27	M.P. nr 30, poz. 414
		/GP		
58	w sprawie prorodzinnego podatku dochodowego od osób fizycznych	Przebieg procesu legislacyjnego564	23-09-1998	Tekst uchwały
		31-07-1998	pos. nr 29	M.P. nr 32, poz. 454
		/ROD		
59	w sprawie zmiany Regulaminu Sejmu Rzeczypospolitej Polskiej	Przebieg procesu legislacyjnego267	30-09-1998	Tekst uchwały
		01-04-1998	pos. nr 30	M.P. nr 34, poz. 483
		/GP		
		Przebieg procesu legislacyjnego349		
		04-05-1998		
		/RSP		
60	w sprawie podjęcia działań służących skutecznemu zwalczaniu wzrastającej przestępczości w państwie	Przebieg procesu legislacyjnego473	30-09-1998	Tekst uchwały
		01-07-1998	pos. nr 30	M.P. nr 34, poz. 482
		/GP		
61	w sprawie budowy w Warszawie Świątyni Opatrzności Bożej	Przebieg procesu legislacyjnego492	23-10-1998	Tekst uchwały
		02-07-1998	pos. nr 32	M.P. nr 38, poz. 519
		/GP		
62	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego718	20-11-1998	Tekst uchwały
		19-11-1998	pos. nr 35	Niepublikowana
		/PS		
63	w sprawie wyboru składu osobowego Komisji do Spraw Kontroli Państwowej	Przebieg procesu legislacyjnego719	20-11-1998	Tekst uchwały
		19-11-1998	pos. nr 35	Niepublikowana
		/PS		
64	w sprawie 76 rocznicy zamordowania pierwszego Prezydenta Rzeczypospolitej Polskiej Gabriela Narutowicza	Przebieg procesu legislacyjnego376	03-12-1998	Tekst uchwały
		16-12-1997	pos. nr 37	M.P. nr 45, poz. 623
		/GP		
65	w 50 rocznicę przyjęcia Powszechnej Deklaracji Praw Człowieka	Przebieg procesu legislacyjnego632	10-12-1998	Tekst uchwały
		30-09-1998	pos. nr 38	M.P. nr 45, poz. 624
		/ROD, SPC, SZA		
66	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia rządowego projektu ustawy o ochronie informacji niejawnych	Przebieg procesu legislacyjnego784	10-12-1998	Tekst uchwały
		10-12-1998	pos. nr 38	Niepublikowana
		/PS		
67	w sprawie wyboru sędziego Trybunału Konstytucyjnego	Przebieg procesu legislacyjnego765	18-12-1998	Tekst uchwały
		03-12-1998	pos. nr 39	M.P. nr 46, poz. 646
		/GP		
68	w sprawie uczczenia 80-tej rocznicy wybuchu Powstania Wielkopolskiego 1918-1919	Przebieg procesu legislacyjnego827	29-12-1998	Tekst uchwały
		18-12-1998	pos. nr 40	M.P. nr 46, poz. 647
		/GP		
69	o uczczeniu pamięci Romana Dmowskiego	Przebieg procesu legislacyjnego783	08-01-1999	Tekst uchwały
		02-12-1998	pos. nr 41	M.P. nr 3, poz. 12
		/GP		
70	zmieniająca uchwałę w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących powszechnego dostępu do archiwów i dokumentacji byłych organów bezpieczeństwa państwa	Przebieg procesu legislacyjnego870	21-01-1999	Tekst uchwały
		19-01-1999	pos. nr 43	Niepublikowana
		/PS		
71	Posłanie Sejmu Rzeczypospolitej Polskiej do Narodu Białoruskiego	Przebieg procesu legislacyjnego780	22-01-1999	Tekst uchwały
		02-12-1998	pos. nr 43	M.P. nr 4, poz. 16
		/GP		
72	w sprawie uwięzienia przez władze Chińskiej Republiki Ludowej działaczy opozycji demokratycznej	Przebieg procesu legislacyjnego847	22-01-1999	Tekst uchwały
		30-12-1998	pos. nr 43	M.P. nr 4, poz. 15
		/GP		
73	zmiany w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego883	22-01-1999	Tekst uchwały
		21-01-1999	pos. nr 43	Niepublikowana
		/PS		
74	z okazji 80-tej rocznicy zwołania Sejmu Ustawodawczego	Przebieg procesu legislacyjnego878	17-02-1999	Tekst uchwały
		19-01-1999	pos. nr 44	M.P. nr 6, poz. 64
		/PS		
75	w sprawie uzupełnienia składu osobowego Komisji do Spraw Służb Specjalnych	Przebieg procesu legislacyjnego923	19-02-1999	Tekst uchwały
		17-02-1999	pos. nr 44	Niepublikowana
		/PS		
76	Rezolucja w sprawie przedstawienia przez Radę Ministrów strategii zrównoważonego rozwoju Polski	Przebieg procesu legislacyjnego848	02-03-1999	Tekst uchwały
		29-12-1998	pos. nr 45	M.P. nr 8, poz. 96
		/OSZ		
77	w sprawie zmiany Regulaminu Sejmu Rzeczypospolitej Polskiej	Przebieg procesu legislacyjnego691	19-03-1999	Tekst uchwały
		23-10-1998	pos. nr 46	M.P. nr 11, poz. 150
		/GP		
78	w sprawie realizacji polityki kulturalnej państwa	Przebieg procesu legislacyjnego974	08-04-1999	Tekst uchwały
		05-03-1999	pos. nr 47	M.P. nr 13, poz. 174
		/KSP		
79	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących problematyki ubezpieczeń społecznych	Przebieg procesu legislacyjnego1038	09-04-1999	Tekst uchwały
		08-04-1999	pos. nr 47	Niepublikowana
		/PS		
80	zmiany w składzie osobowym Komisji do Spraw Służb Specjalnych	Przebieg procesu legislacyjnego1036	10-04-1999	Tekst uchwały
		08-04-1999	pos. nr 47	Niepublikowana
		/PS		
81	zmiany w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego1037	10-04-1999	Tekst uchwały
		08-04-1999	pos. nr 47	Niepublikowana
		/PS		
82	w sprawie powołania członków Krajowej Rady Radiofonii i Telewizji	Przebieg procesu legislacyjnego961	21-04-1999	Tekst uchwały
		05-03-1999	pos. nr 48	M.P. nr 15, poz. 196
		/GP		
83	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących wynagrodzenia osób pełniących funkcje publiczne, a także osób zatrudnionych w niektórych zakładach pracy	Przebieg procesu legislacyjnego1039	22-04-1999	Tekst uchwały
		21-04-1999	pos. nr 48	Niepublikowana
		/PS		
84	Rezolucja w sprawie uruchomienia Terminalu samochodowego odpraw celnych w Koroszczynie	Przebieg procesu legislacyjnego991	22-04-1999	Tekst uchwały
		16-03-1999	pos. nr 48	M.P. nr 15, poz. 197
		/GP		
85	o zmianie uchwały w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących problematyki ubezpieczeń społecznych	Przebieg procesu legislacyjnego1088	07-05-1999	Tekst uchwały
		04-05-1999	pos. nr 49	Niepublikowana
		/PS		
86	w 50. rocznicę utworzenia Rady Europy	Przebieg procesu legislacyjnego1100	21-05-1999	Tekst uchwały
		06-05-1999	pos. nr 50	M.P. nr 18, poz. 241
		/GP		
87	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego1131	21-05-1999	Tekst uchwały
		11-05-1999	pos. nr 50	Niepublikowana
		/PS		
88	Rezolucja w sprawie przedstawienia Sejmowi Rzeczypospolitej Polskiej sprawozdania z funkcjonowania ustawy o wspieraniu przedsięwzięć termomodernizacyjnych	Przebieg procesu legislacyjnego1127	18-06-1999	Tekst uchwały
		09-04-1999	pos. nr 51	M.P. nr 23, poz. 334
		/PBM		
89	w sprawie sprawozdania Krajowej Rady Radiofonii i Telewizji z rocznej działalności	Przebieg procesu legislacyjnego1078	24-06-1999	Tekst uchwały
		22-04-1999	pos. nr 52	M.P. nr 23, poz. 330
		/KSP		
90	w sprawie realizacji ustaleń zawartych w Protokole z rozmów przedstawicieli Rządu Rzeczypospolitej Polskiej z przedstawicielami Central Związków Rolników	Przebieg procesu legislacyjnego1077	25-06-1999	Tekst uchwały
		21-04-1999	pos. nr 52	M.P. nr 23, poz. 333
		/GP		
91	w sprawie wyboru sędziego Trybunału Konstytucyjnego	Przebieg procesu legislacyjnego1159	25-06-1999	Tekst uchwały
		10-06-1999	pos. nr 52	M.P. nr 23, poz. 332
		/GP		
92	w sprawie zmiany Regulaminu Sejmu Rzeczypospolitej Polskiej	Przebieg procesu legislacyjnego1170	25-06-1999	Tekst uchwały
		17-06-1999	pos. nr 52	M.P. nr 23, poz. 331
		/PS		
93	w sprawie wzrostu wykorzystania energii ze źródeł odnawialnych	Przebieg procesu legislacyjnego1119	08-07-1999	Tekst uchwały
		08-05-1999	pos. nr 54	M.P. nr 25, poz. 365
		/OSZ		
94	w sprawie stanu bezpieczeństwa i porządku publicznego w Polsce	1115-A	22-07-1999	Tekst uchwały
		18-06-1999	pos. nr 55	M.P. nr 26, poz. 390
		/ASW		
95	o zmianie uchwały w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących spółdzielczości mieszkaniowej	Przebieg procesu legislacyjnego1259	22-07-1999	Tekst uchwały
		20-07-1999	pos. nr 55	Niepublikowana
		/PS		
96	w sprawie przyjęcia sprawozdania z działalności Najwyższej Izby Kontroli	Przebieg procesu legislacyjnego1175	23-07-1999	Tekst uchwały
		15-06-1999	pos. nr 55	M.P. nr 26, poz. 351
		/NIK		
97	w sprawie przyjęcia sprawozdania z wykonania budżetu państwa za okres od dnia 1 stycznia do dnia 31 grudnia 1998 r. oraz w sprawie absolutorium dla Rady Ministrów	Przebieg procesu legislacyjnego1244	23-07-1999	Tekst uchwały
		13-07-1999	pos. nr 55	M.P. nr 26, poz. 392
		/FPB		
98	w sprawie wyboru członków Kolegium Instytutu Pamięci Narodowej - Komisji Ścigania Zbrodni przeciwko Narodowi Polskiemu	Przebieg procesu legislacyjnego1243	24-07-1999	Tekst uchwały
		15-07-1999	pos. nr 55	M.P. nr 26, poz. 393
		/GP		
99	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego1273	24-07-1999	Tekst uchwały
		22-07-1999	pos. nr 55	Niepublikowana
		/PS		
100	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego1340	10-09-1999	Tekst uchwały
		09-09-1999	pos. nr 57	Niepublikowana
		/PS		
101	w sprawie pomocy społecznej	Przebieg procesu legislacyjnego1295	23-09-1999	Tekst uchwały
		26-07-1999	pos. nr 58	M.P. nr 31, poz. 467
		/POS, ROD		
102	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektu ustawy o reprywatyzacji nieruchomości i niektórych ruchomości osób fizycznych przejętych przez Państwo lub gminę miasta stołecznego Warszawy oraz o rekompensatach	Przebieg procesu legislacyjnego1417	08-10-1999	Tekst uchwały
		07-10-1999	pos. nr 59	Niepublikowana
		/PS		
103	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektów ordynacji wyborczych do Sejmu i do Senatu oraz o zmianie ustawy o wyborze Prezydenta Rzeczypospolitej Polskiej	Przebieg procesu legislacyjnego1429	14-10-1999	Tekst uchwały
		14-10-1999	pos. nr 60	Niepublikowana
		/PS		
104	w 150. rocznicę śmierci Fryderyka Chopina	Przebieg procesu legislacyjnego1440	20-10-1999	Tekst uchwały
		19-10-1999	pos. nr 61	M.P. nr 34, poz. 522
		/PS		
105	w sprawie zmiany w składzie osobowym Komisji do Spraw Służb Specjalnych	Przebieg procesu legislacyjnego1463	22-10-1999	Tekst uchwały
		22-10-1999	pos. nr 61	Niepublikowana
		/PS		
106	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego1490	05-11-1999	Tekst uchwały
		04-11-1999	pos. nr 62	Niepublikowana
		/PS		
107	w sprawie zlecenia Najwyższej Izbie Kontroli przeprowadzenia kontroli działalności Krajowej Rady Radiofonii i Telewizji oraz innych odpowiednich organów w zakresie realizacji koncesji dla Radia Maryja	Przebieg procesu legislacyjnego1439	15-12-1999	Tekst uchwały
		06-10-1999	pos. nr 66	Niepublikowana
		/GP		
108	w sprawie uzupełnienia składu osobowego Komisji do Spraw Służb Specjalnych	Przebieg procesu legislacyjnego1563	15-12-1999	Tekst uchwały
		14-12-1999	pos. nr 66	Niepublikowana
		/PS		
109	w sprawie sytuacji w polskim cukrownictwie i prywatyzacji przemysłu cukrowniczego	Przebieg procesu legislacyjnego1364	22-12-1999	Tekst uchwały
		09-09-1999	pos. nr 67	M.P. nr 41, poz. 620
		/GP		
110	w sprawie transmisji w Polskim Radiu S.A. obrad plenarnych Sejmu	Przebieg procesu legislacyjnego1612	23-12-1999	Tekst uchwały
		22-12-1999	pos. nr 68	M.P. nr 41, poz. 621
		/KSP		
111	w sprawie uczczenia 75. rocznicy śmierci Władysława Stanisława Reymonta	Przebieg procesu legislacyjnego1567	05-01-2000	Tekst uchwały
		06-12-1999	pos. nr 69	M.P. nr 1, poz. 1
		/GP		
112	w sprawie wzmocnienia kontroli państwowej prowadzonej przez Najwyższą Izbę Kontroli	Przebieg procesu legislacyjnego1238	20-01-2000	Tekst uchwały
		08-07-1999	pos. nr 70	M.P. nr 2, poz. 10
		/KOP		
113	"""Polska Deklaracja w Sprawie Młodzieży i Alkoholu"""	Przebieg procesu legislacyjnego1625	18-02-2000	Tekst uchwały
		24-12-1999	pos. nr 71	M.P. nr 6, poz. 125
		/ROD, ZDR		
114	w sprawie przygotowań do członkostwa Rzeczypospolitej Polskiej w Unii Europejskiej	Przebieg procesu legislacyjnego1685	18-02-2000	Tekst uchwały
		02-02-2000	pos. nr 71	M.P. nr 6, poz. 124
		/INE		
115	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących funkcjonowania banków spółdzielczych	Przebieg procesu legislacyjnego1731	18-02-2000	Tekst uchwały
		17-02-2000	pos. nr 71	Niepublikowana
		/PS		
116	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego1732	18-02-2000	Tekst uchwały
		17-02-2000	pos. nr 71	Niepublikowana
		/PS		
117	w sprawie zmiany Regulaminu Sejmu Rzeczypospolitej Polskiej	Przebieg procesu legislacyjnego1715	17-03-2000	Tekst uchwały
		15-02-2000	pos. nr 73	M.P. nr 9, poz. 177
		/PS		
118	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego1796	31-03-2000	Tekst uchwały
		30-03-2000	pos. nr 74	Niepublikowana
		/PS		
119	upamiętniająca 60 rocznicę zbrodni katyńskiej	1811-A	13-04-2000	Tekst uchwały
		12-04-2000	pos. nr 75	M.P. nr 11, poz. 198
		/PS		
120	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia projektu ustawy o zamówieniach publicznych	Przebieg procesu legislacyjnego1830	14-04-2000	Tekst uchwały
		14-04-2000	pos. nr 75	Niepublikowana
		/PS		
121	Rezolucja Sejmu w sprawie polityki mieszkaniowej Państwa w okresie transformacji społeczno-gospodarczej	Przebieg procesu legislacyjnego1802	28-04-2000	Tekst uchwały
		30-03-2000	pos. nr 76	M.P. nr 13, poz. 256
		/PBM		
122	w 1000. rocznicę Zjazdu Gnieźnieńskiego	Przebieg procesu legislacyjnego1833	29-04-2000	Tekst uchwały
		14-04-2000	pos. nr 77	M.P. nr 13, poz. 255
		/PS		
123	w sprawie kierunków prywatyzacji PKO BP, BGŻ oraz PZU S.A.	Przebieg procesu legislacyjnego1875	10-05-2000	Tekst uchwały
		05-04-2000	pos. nr 78	M.P. nr 14, poz. 284
		/GP		
124	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego1953	26-05-2000	Tekst uchwały
		25-05-2000	pos. nr 79	Niepublikowana
		/PS		
125	w sprawie powołania Rzecznika Praw Dziecka	Przebieg procesu legislacyjnego1865	08-06-2000	Tekst uchwały
		14-04-2000	pos. nr 80	M.P. nr 19, poz. 403
		/GP		
126	w sprawie powołania Prezesa Instytutu Pamięci Narodowej - Komisji Ścigania Zbrodni przeciwko Narodowi Polskiemu	Przebieg procesu legislacyjnego1933	08-06-2000	Tekst uchwały
		18-05-2000	pos. nr 80	M.P. nr 19, poz. 402
		/PS		
127	w sprawie powołania Rzecznika Praw Obywatelskich	Przebieg procesu legislacyjnego1935	08-06-2000	Tekst uchwały
		16-05-2000	pos. nr 80	M.P. nr 19, poz. 401
		/GP		
128	w pierwszą rocznicę historycznej wizyty Jana Pawła II w Sejmie i Senacie Rzeczypospolitej Polskiej 11 czerwca 1999 roku	Przebieg procesu legislacyjnego1961	08-06-2000	Tekst uchwały
		25-05-2000	pos. nr 80	M.P. nr 17, poz. 359
		/GP		
129	na 10-lecie odrodzenia polskiego samorządu terytorialnego	Przebieg procesu legislacyjnego1931	29-06-2000	Tekst uchwały
		12-05-2000	pos. nr 81	M.P. nr 19, poz. 404
		/STR		
130	w sprawie sprawozdania Krajowej Rady Radiofonii i Telewizji z rocznej działalności	Przebieg procesu legislacyjnego1924	30-06-2000	Tekst uchwały
		11-05-2000	pos. nr 81	M.P. nr 20, poz. 409
		/KSP		
131	w sprawie zmiany Regulaminu Sejmu Rzeczypospolitej Polskiej	Przebieg procesu legislacyjnego2010	13-07-2000	Tekst uchwały
		28-06-2000	pos. nr 82	M.P. nr 21, poz. 428
		/PS		
132	w sprawie budowania podstaw społeczeństwa informacyjnego w Polsce	Przebieg procesu legislacyjnego2017	14-07-2000	Tekst uchwały
		09-06-2000	pos. nr 82	M.P. nr 22, poz. 448
		/GP		
133	w sprawie przyjęcia sprawozdania z wykonania budżetu państwa za okres od dnia 1 stycznia do dnia 31 grudnia 1999 r. oraz w sprawie absolutorium dla Rady Ministrów	Przebieg procesu legislacyjnego2065	20-07-2000	Tekst uchwały
		13-07-2000	pos. nr 83	M.P. nr 23, poz. 469
		/FPB		
134	w sprawie wyboru składu osobowego komisji nadzwyczajnej - Komisji Prawa Europejskiego	Przebieg procesu legislacyjnego2104	21-07-2000	Tekst uchwały
		21-07-2000	pos. nr 83	Niepublikowana
		/PS		
135	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego2105	21-07-2000	Tekst uchwały
		21-07-2000	pos. nr 83	Niepublikowana
		/PS		
136	"w sprawie uczczenia 20. rocznicy Polskiego Sierpnia i powstania NSZZ ""Solidarność"""	Przebieg procesu legislacyjnego2168	07-09-2000	Tekst uchwały
		05-09-2000	pos. nr 85	M.P. nr 27, poz. 546
		/GP		
137	w sprawie anulowania długów najuboższych krajów świata w Roku Jubileuszu 2000	Przebieg procesu legislacyjnego1604	15-09-2000	Tekst uchwały
		02-09-1999	pos. nr 86	M.P. nr 28, poz. 557
		/GP		
138	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego2123	15-09-2000	Tekst uchwały
		15-09-2000	pos. nr 86	Niepublikowana
		/PS		
139	w sprawie utworzenia Kolegium Europejskiego w Gnieźnie w tysięczną rocznicę Zjazdu Gnieźnieńskiego	Przebieg procesu legislacyjnego1905	22-09-2000	Tekst uchwały
		28-04-2000	pos. nr 87	M.P. nr 29, poz. 594
		/GP		
		Przebieg procesu legislacyjnego1942		
		12-05-2000		
		/GP		
140	w sprawie organizacji Światowej Wystawy Rejestrowanej EXPO 2010 w Polsce	Przebieg procesu legislacyjnego2081	22-09-2000	Tekst uchwały
		13-07-2000	pos. nr 87	M.P. nr 29, poz. 593
		/STR		
141	w sprawie powołania i wyboru składu osobowego Komisji Nadzwyczajnej do rozpatrzenia poselskich projektów ustaw dotyczących prawa obywateli do uzyskiwania informacji o działalności organów władzy publicznej oraz osób pełniących funkcje publiczne, a także dotyczących jawności procedur decyzyjnych i grup interesów	Przebieg procesu legislacyjnego2224	22-09-2000	Tekst uchwały
		22-09-2000	pos. nr 87	Niepublikowana
		/PS		
142	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego2225	22-09-2000	Tekst uchwały
		22-09-2000	pos. nr 87	Niepublikowana
		/PS		
143	w sprawie odwołania Rzecznika Praw Dziecka	Przebieg procesu legislacyjnego2196	12-10-2000	Tekst uchwały
		12-09-2000	pos. nr 88	M.P. nr 35, poz. 710
		/PS		
144	w sprawie uczczenia 100. rocznicy urodzin kardynała Stefana Wyszyńskiego	Przebieg procesu legislacyjnego2064	25-10-2000	Tekst uchwały
		08-06-2000	pos. nr 89	M.P. nr 35, poz. 711
		/GP		
145	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego2337	09-11-2000	Tekst uchwały
		09-11-2000	pos. nr 90	Niepublikowana
		/PS		
146	w sprawie uczczenia 60. rocznicy powstania Batalionów Chłopskich	Przebieg procesu legislacyjnego2334	30-11-2000	Tekst uchwały
		26-10-2000	pos. nr 92	M.P. nr 40, poz. 775
		/GP		
147	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego2415	30-11-2000	Tekst uchwały
		30-11-2000	pos. nr 92	Niepublikowana
		/PS		
148	w sprawie ustanowienia roku 2001 rokiem Ignacego Jana Paderewskiego	Przebieg procesu legislacyjnego2293	07-12-2000	Tekst uchwały
		13-10-2000	pos. nr 93	M.P. nr 42, poz. 803
		/GP		
149	w sprawie powołania Rzecznika Praw Dziecka	Przebieg procesu legislacyjnego2385	08-12-2000	Tekst uchwały
		16-11-2000	pos. nr 93	Niepublikowana
		/GP		
150	w sprawie postępowania z projektem ustawy zmieniającej ustawę o zmianie ustawy - Kodeks karny wykonawczy (druk nr 2442)	Przebieg procesu legislacyjnego2443	08-12-2000	Tekst uchwały
		07-12-2000	pos. nr 93	Niepublikowana
		/PS		
151	w sprawie zmiany Regulaminu Sejmu Rzeczypospolitej Polskiej	Przebieg procesu legislacyjnego2267	15-12-2000	Tekst uchwały
		10-10-2000	pos. nr 94	M.P. nr 42, poz. 804
		/PS		
152	"Rezolucja w sprawie rządowego dokumentu ""Koncepcja Polityki Przestrzennego Zagospodarowania Kraju"""	Przebieg procesu legislacyjnego2159	21-12-2000	Tekst uchwały
		26-07-2000	pos. nr 95	M.P. nr 43, poz. 834
		/OSZ, PBM, STR		
153	w sprawie realizacji przedsięwzięcia inwestycyjnego pod nazwą Stopień wodny Nieszawa - Ciechocinek	Przebieg procesu legislacyjnego1505	22-12-2000	Tekst uchwały
		05-11-1999	pos. nr 95	M.P. nr 43, poz. 832
		/GP		
154	w sprawie powołania Prezesa Narodowego Banku Polskiego	Przebieg procesu legislacyjnego2447	22-12-2000	Tekst uchwały
		08-12-2000	pos. nr 95	M.P. nr 43, poz. 833
		/Prezydent		
155	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego2502	22-12-2000	Tekst uchwały
		22-12-2000	pos. nr 95	Niepublikowana
		/PS		
156	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego2556	18-01-2001	Tekst uchwały
		18-01-2001	pos. nr 98	Niepublikowana
		/PS		
157	w sprawie wyboru Rzecznika Praw Dziecka	Przebieg procesu legislacyjnego2543	01-02-2001	Tekst uchwały
		11-01-2001	pos. nr 99	M.P. nr 7, poz. 109
		/GP		
158	w 170. rocznicę bitwy o Olszynkę Grochowską	Przebieg procesu legislacyjnego2624	16-02-2001	Tekst uchwały
		13-02-2001	pos. nr 101	M.P. nr 7, poz. 110
		/GP		
159	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego2626	16-02-2001	Tekst uchwały
		16-02-2001	pos. nr 101	Niepublikowana
		/PS		
160	w sprawie uzupełnienia składu osobowego Komisji do Spraw Służb Specjalnych	Przebieg procesu legislacyjnego2627	16-02-2001	Tekst uchwały
		16-02-2001	pos. nr 101	Niepublikowana
		/PS		
161	w sprawie wyboru sędziego Trybunału Konstytucyjnego	Przebieg procesu legislacyjnego2565	02-03-2001	Tekst uchwały
		19-01-2001	pos. nr 102	M.P. nr 9, poz. 149
		/GP		
162	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego2669	07-03-2001	Tekst uchwały
		02-03-2001	pos. nr 103	Niepublikowana
		/PS		
163	"w sprawie hołdu poległym, pomordowanym i prześladowanym członkom organizacji ,,Wolność i Niezawisłość"""	Przebieg procesu legislacyjnego2636	14-03-2001	Tekst uchwały
		14-02-2001	pos. nr 104	M.P. nr 10, poz. 157
		/GP		
164	w sprawie wyboru posła - członka Krajowej Rady Sądownictwa	Przebieg procesu legislacyjnego2679	28-03-2001	Tekst uchwały
		12-03-2001	pos. nr 105	M.P. nr 11, poz. 167
		/GP		
165	w sprawie wyboru członka Krajowej Rady Radiofonii i Telewizji	Przebieg procesu legislacyjnego2675	12-04-2001	Tekst uchwały
		03-03-2001	pos. 106	M.P. nr 12, poz. 188
		/GP		
166	w sprawie nierównoprawnego traktowania nadawców radiowych przez Krajową Radę Radiofonii i Telewizji w związku z realizacją koncesji radiowych	Przebieg procesu legislacyjnego2333	26-04-2001	Tekst uchwały
		27-10-2000	pos. nr 107	M.P. nr 14, poz. 220
		/KSP		
		Przebieg procesu legislacyjnego2434		
		30-11-2000		
		/GP		
167	w sprawie wyboru członka Kolegium Instytutu Pamięci Narodowej - Komisji Ścigania Zbrodni przeciwko Narodowi Polskiemu	Przebieg procesu legislacyjnego2765	26-04-2001	Tekst uchwały
		15-03-2001	pos. nr 107	M.P. nr 15, poz. 241
		/GP		
168	w 80 rocznicę III Powstania Śląskiego	Przebieg procesu legislacyjnego2784	26-04-2001	Tekst uchwały
		11-04-2001	pos. nr 107	M.P. nr 14, poz. 219
		/PS		
169	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego2839	27-04-2001	Tekst uchwały
		27-04-2001	pos. nr 107	Niepublikowana
		/PS		
170	w sprawie oceny funkcjonowania zasadniczego podziału terytorialnego państwa	Przebieg procesu legislacyjnego2634	11-05-2001	Tekst uchwały
		14-02-2001	pos. nr 108	M.P. nr 16, poz. 249
		/ASW, STR		
171	w sprawie oddania hołdu Dzieciom Wrzesińskim w setną rocznicę strajku	Przebieg procesu legislacyjnego2913	25-05-2001	Tekst uchwały
		10-04-2001	pos. nr 109	M.P. nr 18, poz. 298
		/GP		
172	w sprawie uzupełnienia składu osobowego Komisji Etyki Poselskiej	Przebieg procesu legislacyjnego2935	25-05-2001	Tekst uchwały
		25-05-2001	pos. nr 109	Niepublikowana
		/PS		
173	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego2948	25-05-2001	Tekst uchwały
		25-05-2001	pos. nr 109	Niepublikowana
		/PS		
174	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego3026	08-06-2001	Tekst uchwały
		08-06-2001	pos. nr 110	Niepublikowana
		/PS		
175	w 25 rocznicę protestu w Radomiu, Ursusie, Płocku	Przebieg procesu legislacyjnego3075	21-06-2001	Tekst uchwały
		10-05-2001	pos. nr 111	M.P. nr 21, poz. 327
		/GP		
176	oddająca hołd uczestnikom Poznańskiego Czerwca	Przebieg procesu legislacyjnego3074	22-06-2001	Tekst uchwały
		10-05-2001	pos. nr 111	M.P. nr 21, poz. 328
		/GP		
177	z okazji 10 - lecia podpisania Traktatu o dobrym sąsiedztwie i przyjaznej współpracy między Rzecząpospolitą Polską i Republiką Federalną Niemiec	Przebieg procesu legislacyjnego3082	22-06-2001	Tekst uchwały
		19-06-2001	pos. nr 111	M.P. nr 21, poz. 329
		/SZA		
178	w sprawie powołania Prezesa Najwyższej Izby Kontroli	Przebieg procesu legislacyjnego2969	06-07-2001	Tekst uchwały
		28-05-2001	pos. nr 112	M.P. nr 23, poz. 395
		/GP		
179	w sprawie Narodowej Fundacji na rzecz Kahlenbergu	Przebieg procesu legislacyjnego2995	19-07-2001	Tekst uchwały
		25-05-2001	pos. nr 113	M.P. nr 23, poz. 396
		/GP		
180	Rezolucja w sprawie planu realizacji wydatków budżetu państwa w roku 2001	Przebieg procesu legislacyjnego3194	20-07-2001	Tekst uchwały
		17-07-2001	pos. nr 113	
		/GP		
181	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego3211	20-07-2001	Tekst uchwały
		20-07-2001	pos. nr 113	Niepublikowana
		/PS		
182	w sprawie zmian w składzie osobowym Komisji Etyki Poselskiej	Przebieg procesu legislacyjnego3212	20-07-2001	Tekst uchwały
		20-07-2001	pos. nr 113	Niepublikowana
		/PS		
183	w sprawie przyjęcia sprawozdania z wykonania budżetu państwa za okres od dnia 1 stycznia do 31 grudnia 2000 r. oraz w sprawie absolutorium dla Rady Ministrów	Przebieg procesu legislacyjnego2982	27-07-2001	Tekst uchwały
		31-05-2001	pos. nr 114	M.P. nr 24, poz. 414
		/RM		
184	w sprawie sprawozdania Krajowej Rady Radiofonii i Telewizji z rocznej działalności	Przebieg procesu legislacyjnego3107	27-07-2001	Tekst uchwały
		21-06-2001	pos. nr 114	M.P. nr 24, poz. 415
		/KSP		
185	w sprawie upamiętnienia ofiar Konzentrationslager Warschau	Przebieg procesu legislacyjnego3262	27-07-2001	Tekst uchwały
		02-05-2001	pos. nr 114	M.P. nr 24, poz. 413
		/GP		
186	w 60. rocznicę powstania Armii Polskiej na Wschodzie	Przebieg procesu legislacyjnego3319	22-08-2001	Tekst uchwały
		13-08-2001	pos. nr 117	M.P. nr 29, poz. 487
		/GP		
187	Deklaracja w sprawie solidarności Sejmu Rzeczypospolitej Polskiej z narodem tybetańskim	Przebieg procesu legislacyjnego2157	24-08-2001	Tekst uchwały
		26-07-2000	pos. nr 117	M.P. nr 29, poz. 489
		/GP		
188	w sprawie powszechnej dostępności obywateli do Programu I Polskiego Radia	Przebieg procesu legislacyjnego3318	24-08-2001	Tekst uchwały
		27-07-2001	pos. nr 117	M.P. nr 29, poz. 488
		/GP		
189	w sprawie zmian w składach osobowych komisji sejmowych	Przebieg procesu legislacyjnego3361	25-08-2001	Tekst uchwały
		24-08-2001	pos. nr 117	Niepublikowana
		/PS		
190	Oświadczenie w związku z atakiem terrorystycznym, który nastąpił 11 września 2001 r. na terytorium Stanów Zjednoczonych Ameryki	Przebieg procesu legislacyjnego3427	18-09-2001	Tekst uchwały
		14-09-2001	pos. nr 119	M.P. nr 32, poz. 526
		/GP		