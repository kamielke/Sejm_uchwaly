﻿Lp.	Tytuł	Data uchwalenia	Ogłoszono w Monitorze Polskim
1	w sprawie wyboru Mikołaja Kozakiewicza na stanowisko Marszałka Sejmu	1989-07-04	M.P. 1989 nr 22, poz. 155
		pos. nr 1	
2	w sprawie wyboru na stanowisko Wicemarszałka Sejmu Teresy Dobielińskiej-Eliszewskiej, Tadeusza Fiszbacha i Olgi Krzyżanowskiej	1989-07-04	M.P. 1989 nr 22, poz. 156
		pos. nr 1	
3	w sprawie wyboru Komisji Nadzwyczajnej do rozpatrzenia sprawozdania Państwowej Komisji Wyborczej z wyborów Sejmu oraz dekretów Rady Państwa	1989-07-04	niepublikowana
		pos. nr 1	
4	w sprawie wyboru na sekretarzy Sejmu posłów Barbary Blidy, Radosława Gawlika, Ziemowita Dionizego Gawskiego, Anny Dynowskiej, Adama Grabowieckiego, Krzysztofa Grzebyka, Romualdy Matusiak, Stefana Mleczko, Edwarda Mullera, Waldemara Pawlaka, Jacka Piechoty, Macieja Półtoraka, Marka Rusakiewicza, Marka Rutkowskiego	1989-07-04	niepublikowana
		pos. nr 1	
5	w sprawie zatwierdzenia dekretu z 12 czerwca 1989 r. o zmianie ustawy ordynacja wyborcza do Sejmu PRL X kadencji na lata 1989-1993 oraz dekretu z 1 lipca 1989 r. o zmianie ustawy o obowiązkach i prawach posłów na Sejm	1989-07-05	M.P. 1989 nr 22, poz. 158
		pos. nr 1	
6	w sprawie ważności wyboru posłów na Sejm PRL	1989-07-05	M.P. 1989 nr 22, poz. 157
		pos. nr 1	
7	w sprawie wyboru Trybunału Stanu	1989-07-05	M.P. 1989 nr 22, poz. 159
		pos. nr 1	
8	w sprawie wyboru Komisji Nadzwyczajnej do rozpatrzenia rządowego projektu ustawy o podwyższaniu w 1989 r. wynagrodzeń za pracę w związku ze zmianami cen detalicznych i usług konsumpcyjnych na Sejm PRL	1989-07-10	niepublikowana
		pos. nr 3	
9	w sprawie aktualnej sytuacji dochodowej ludności	1989-07-31	M.P. 1989 nr 26, poz. 201
		pos. nr 4	
10	w sprawie zmiany regulaminu Sejmu PRL	1989-07-31	M.P. 1989 nr 26, poz. 202
		pos. nr 4	
11	w sprawie przyjęcia zgłoszonego przez Prezesa RM ustąpienia rządu i powierzenia dalszego pełnienia funkcji do czasu powołania Rady Ministrów w nowym składzie	1989-08-01	M.P. 1989 nr 26, poz. 203
		pos. nr 4	
12	w sprawie powołania Komisji Nadzwyczajnej ds. przeanalizowania i oceny działalności ustępującego rządu	1989-08-02	niepublikowana
		pos. nr 4	
13	w sprawie powołania Komisji Nadzwyczajnej do zbadania działalności Ministerstwa Spraw Wewnętrznych	1989-08-02	niepublikowana
		pos. nr 4	
14	w sprawie powołania Czesława Kiszczaka na stanowisko Prezesa Rady Ministrów i powierzenia mu przedstawienia wniosków co do składu Rady Ministrów	1989-08-02	M.P. 1989 nr 26, poz. 204
		pos. nr 4	
15	w sprawie wyboru składu stałych komisji sejmowych	1989-08-02	niepublikowana
		pos. nr 4	
16	w sprawie wyboru składu Komisji Nadzwyczajnej do spraw przeanalizowania i oceny działalności ustępującego rządu	1989-08-02	niepublikowana
		pos. nr 4	
17	w sprawie wyboru składu Komisji Nadzwyczajnej do zbadania działalności MSW	1989-08-17	niepublikowana
		pos. nr 5	
18	oświadczenie Sejmu PRL w dwudziestą pierwszą rocznicę interwencji wojsk Układu Warszawskiego w Czechosłowackiej Republice Socjalistycznej	1989-08-17	niepublikowana
		pos. nr 5	
19	oświadczenie Sejmu PRL w związku z 50 rocznicą podpisania paktu Ribbentrop-Mołotow	1989-08-23	niepublikowana
		pos. nr 6	
20	w sprawie odwołania Czesława Kiszczaka ze stanowiska Prezesa Rady Ministrów oraz z misji utworzenia rządu	1989-08-24	M.P. 1989 nr 30, poz. 223
		pos. nr 6	
21	w sprawie powołania Tadeusza Mazowieckiego na stanowisko Prezesa Rady Ministrów	1989-08-24	M.P. 1989 nr 30, poz. 224
		pos. nr 6	
22	oświadczenie Sejmu PRL w związku z 50 rocznicą wybuchu II wojny światowej	1989-08-24	niepublikowana
		pos. nr 6	
23	w sprawie powołania członków Rady Ministrów	1989-09-12	M.P. 1989 nr 32, poz. 243
		pos. nr 7	
24	w sprawie odwołania Zdzisława Pakuły ze stanowiska prezesa NBP	1989-09-12	M.P. 1989 nr 32, poz. 244
		pos. nr 7	
25	w sprawie powołania Władysława Baki na stanowisko prezesa NBP	1989-09-12	M.P. 1989 nr 32, poz. 245
		pos. nr 7	
26	w sprawie zmian w składzie osobowym Komisji Sejmowych	1989-10-06	niepublikowana
		pos. nr 9	
27	w sprawie szczególnego trybu skierowania projektu ustawy o zmianie ustawy budżetowej na 1989 r.	1989-10-20	niepublikowana
		pos. nr 10	
28	w sprawie skierowania projektu ustawy o zmianie ustawy budżetowej do pierwszego czytania na posiedzeniu Komisji	1989-10-20	niepublikowana
		pos. nr 10	
29	w sprawie niektórych zasad realizacji budżetu państwa w 1989 r.	1989-10-30	M.P. 1989 nr 37, poz. 290
		pos. nr 11	
30	w sprawie wyboru Mieczysława Tyczki na stanowisko prezesa Trybunału Konstytucyjnego	1989-11-17	niepublikowana
		pos. nr 12	
31	w sprawie wyboru Tomasza Dybowskiego, Antoniego Filca, Wojciecha Łączkowskiego, Janiny Zakrzewskiej i Andrzeja Zolla na stanowisko sędziego Trybunału Konstytucyjnego	1989-11-17	niepublikowana
		pos. nr 12	
32	w sprawie protestów wyborczych wniesionych przeciwko wyborowi posła w wyborach do Sejmu X Kadencji	1989-12-01	M.P. 1989 nr 40, poz. 314
		pos. nr 14	
33	w sprawie przeprowadzenia wyborów uzupełniających	1989-12-01	M.P. 1989 nr 40, poz. 315
		pos. nr 14	
34	rezolucja w sprawie przedstawienia kierunków rozwoju i stanu rolnictwa oraz polityki rządu wobec rolnictwa i gospodarki żywnościowej	1989-12-01	niepublikowana
		pos. nr 14	
35	o planie Centralnego Funduszu Rozwoju Nauki i Techniki na 1989 r.	1989-12-01	niepublikowana
		pos. nr 14	
36	w sprawie przyjęcia do wiadomości oświadczenia Ministra Spraw Zagranicznych w sprawie aktualnej dyskusji na temat zjednoczenia dwóch państw niemieckich	1989-12-07	niepublikowana
		pos. nr 15	
37	w sprawie powołania Komisji Konstytucyjnej oraz wyboru jej składu	1989-12-07	niepublikowana
		pos. nr 15	
38	w sprawie powołania Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw związanych ze stabilizacją gospodarczą oraz ze zmianami systemowymi w gospodarce	1989-12-17	niepublikowana
		pos. nr 16	
39	w sprawie wydarzeń w Rumunii	1989-12-19	M.P. 1989 nr 43, poz. 342
		pos. nr 16	
40	w sprawie odwołania Marka Kucharskiego ze stanowiska Ministra - członka Rady Ministrów i powołanie go na stanowisko Ministra Łączności	1989-12-20	M.P. 1989 nr 43, poz. 344
		pos. nr 16	
41	w sprawie rządowych założeń polityki rolnej na 1990 rok	1989-12-20	M.P. 1989 nr 43, poz. 343
		pos. nr 16	
42	w sprawie majątku partii i stronnictw politycznych	1990-01-26	M.P. 1990 nr 4, poz. 28
		pos. nr 19	
43	w sprawie powołania Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących samorządu terytorialnego	1990-01-26	niepublikowana
		pos. nr 19	
44	w sprawie wyboru Józefa Lubienieckiego do składu Krajowej Rady Sądownictwa	1990-01-26	niepublikowana
		pos. nr 19	
45	w sprawie wyboru Antoniego Pieniążka, Janusza Dobrosza i Edwarda Rzepki do Krajowej Rady Sądownictwa	1990-02-01	niepublikowana
		pos. nr 20	
46	w sprawie bilansu płatniczego państwa na 1990 r.	1990-02-22	M.P. 1990 nr 10, poz. 71
		pos. nr 22	
47	w sprawie bilansu pieniężnych dochodów i wydatków ludności w 1990 r.	1990-02-22	M.P. 1990 nr 10, poz. 72
		pos. nr 22	
48	w sprawie założeń polityki pieniężnej na 1990 r.	1990-02-22	M.P. 1990 nr 10, poz. 73
		pos. nr 22	
49	w sprawie planu Centralnego Funduszu Rozwoju Kultury na 1990 r.	1990-02-22	M.P. 1990 nr 10, poz. 74
		pos. nr 22	
50	w sprawie planu Centralnego Funduszu Rozwoju Nauki i Techniki na 1990 r.	1990-02-22	M.P. 1990 nr 10, poz. 75
		pos. nr 22	
51	w sprawie zmian w NPSG na lata 1986-1990	1990-02-22	M.P. 1990 nr 13, poz. 83
		pos. nr 22	
52	w sprawie realizacji ustawy budżetowej na rok 1990	1990-02-23	M.P. 1990 nr 8, poz. 55
		pos. nr 22	
53	w sprawie założeń polityki społeczno-gospodarczej na 1990 r.	1990-02-23	M.P. 1990 nr 8, poz. 54
		pos. nr 22	
54	rezolucja w sprawie ustawy o ubezpieczeniu społecznym rolników	1990-02-24	niepublikowana
		pos. nr 22	
55	w sprawie stwierdzenia ważności wyborów uzupełniających do Sejmu w dniu 11 lutego 1990 r.	1990-03-08	M.P. 1990 nr 10, poz. 76
		pos. nr 23	
56	w sprawie zmian w składzie osobowym komisji sejmowych	1990-03-09	niepublikowana
		pos. nr 23	
57	w sprawie Litwy	1990-03-22	M.P. 1990 nr 11, poz. 79
		pos. nr 24	
58	w sprawie Katynia	1990-03-22	M.P. 1990 nr 11, poz. 80
		pos. nr 24	
59	w sprawie przyjęcia do wiadomości sprawozdania Rzecznika Praw Obywatelskich z działalności w 1989 r.	1990-03-24	niepublikowana
		pos. nr 24	
60	w sprawie powołania Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących zmian systemowych w gospodarce	1990-04-05	niepublikowana
		pos. nr 25	
61	zmieniająca regulamin Sejmu RP	1990-04-18	M.P. 1990 nr 16, poz. 121
		pos. nr 27	
62	w sprawie uznania za zasadne orzeczeń Trybunału Konstytucyjnego dotyczących ustaw: o zaopatrzeniu emerytalnym pracowników i ich rodzin, o zaopatrzeniu pracowników kolejowych i ich rodzin oraz o zaopatrzeniu emerytalnym górników i ich rodzin	1990-04-27	niepublikowana
		pos. nr 28	
63	w sprawie przyjęcia informacji Ministra Spraw Zagranicznych do wiadomości	1990-04-27	niepublikowana
		pos. nr 28	
64	w sprawie emerytur i rent ze starego portfela	1990-04-27	M.P. 1990 nr 18, poz. 137
		pos. nr 28	
65	w sprawie Katynia	1990-04-28	M.P. 1990 nr 18, poz. 136
		pos. nr 28	
66	w sprawie racjonalnego gospodarowania naturalnymi zasobami mineralnymi	1990-05-10	M.P. 1990 nr 19, poz. 145
		pos. nr 29	
67	w sprawie uznania za zasadne orzeczenia Trybunału Konstytucyjnego dotyczącego ustawy o cenach	1990-05-17	niepublikowana
		pos. nr 30	
68	rezolucja w sprawie uproszczenia i ograniczenia sprawozdawczości i badań statystycznych	1990-05-18	M.P. 1990 nr 20, poz. 158
		pos. nr 30	
69	w sprawie przyjęcia do wiadomości sprawozdania Komisji Sprawiedliwości o sprawozdaniu Komisji Rządowej ds. ustalenia stanu prawnego majątku partii politycznych i organizacji młodzieżowych (...) oraz przyjęcia do wiadomości stanowiska rządu w tej sprawie	1990-05-19	niepublikowana
		pos. nr 29	
70	w sprawie wprowadzania w życie podziału zadań i kompetencji pomiędzy organy gminy a organy administracji rządowej	1990-05-24	M.P. 1990 nr 21, poz. 164
		pos. nr 31	
71	o zmianie uchwały Sejmu RP w sprawie realizacji ustawy budżetowej na rok 1990	1990-05-24	M.P. 1990 nr 21, poz. 165
		pos. nr 31	
72	dotycząca spraw niemieckich	1990-06-08	M.P. 1990 nr 24, poz. 179
		pos. nr 32	
73	w sprawie działania podmiotów gospodarczych na rzecz ochrony środowiska	1990-06-21	niepublikowana
		pos. nr 33	
74	w sprawie wyboru I Prezesa Sądu Najwższego	1990-06-22	M.P. 1990 nr 25, poz. 190
		pos. nr 33	
75	w sprawie powołania Komisji Nadzwyczajnej do zbadania działań policji wobec rolników protestujących w Mławie oraz w gmachu Ministerstwa Rolnictwa i Gospodarki Żywnościowej a także w stosunku do obecnych w gmachu Ministerstwa posłów	1990-07-05	niepublikowana
		pos. nr 34	
76	w sprawie wyboru składu osobowego Komisji Nadzwyczajnej do zbadania działań policji wobec rolników protestujących w Mławie oraz w gmachu Ministerstwa Rolnictwa i Gospodarki Żywnościowej a także w stosunku do obecnych w gmachu Ministerstwa posłów	1990-07-05	niepublikowana
		pos. nr 34	
77	w sprawie odwołania Czesława Janickiego ze stanowiska Wiceprezesa Rady Ministrów i Ministra Rolnictwa i Gospodarki Żywnościowej	1990-07-06	M.P. 1990 nr 27, poz. 209
		pos. nr 34	
78	w sprawie odwołania Czesława Kiszczaka ze stanowiska Wiceprezesa Rady Ministrów i Ministra Spraw Wewnętrznych	1990-07-06	M.P. 1990 nr 27, poz. 210
		pos. nr 34	
79	w sprawie odwołania Floriana Siwickiego ze stanowiska Ministra Obrony Narodowej	1990-07-06	M.P. 1990 nr 27, poz. 211
		pos. nr 34	
80	w sprawie odwołania Franciszka Adama Wielądka ze stanowiska Ministra Transportu i Gospodarki Morskiej	1990-07-06	M.P. 1990 nr 27, poz. 212
		pos. nr 34	
81	w sprawie powołania Piotra Kołodziejczyka na stanowisko Ministra Obrony Narodowej	1990-07-06	M.P. 1990 nr 27, poz. 213
		pos. nr 34	
82	w sprawie powołania Krzysztofa Kozłowskiego na stanowisko Ministra Spraw Wewnętrznych	1990-07-06	M.P. 1990 nr 27, poz. 214
		pos. nr 34	
83	w sprawie powołania Ewarysta Waligórskiego na stanowisko Ministra Transportu i Gospodarki Morskiej	1990-07-06	M.P. 1990 nr 27, poz. 215
		pos. nr 34	
84	w sprawie przyjęcia do wiadomości wystąpienia Prezesa Rady Ministrów Tadeusza Mazowieckiego	1990-07-06	niepublikowana
		pos. nr 34	
85	w sprawie nadzoru nad działalnością ubezpieczeniową	1990-07-13	niepublikowana
		pos. nr 35	
86	o likwidacji związków spółdzielczych	1990-07-21	M.P. 1990 nr 30, poz. 230
		pos. nr 36	
87	w sprawie zmiany regulaminu Sejmu	1990-07-21	M.P. 1990 nr 30, poz. 229
		pos. nr 36	
88	w sprawie uznania za zasadne orzeczenia Trybunału Konstytucyjnego stwierdzającego niezgodność z Konstytucją niektórych przepisów ustawy o gospodarce gruntami i wywłaszczaniu nieruchomości	1990-07-26	niepublikowana
		pos. nr 37	
89	w sprawie oddalenia orzeczenia Trybunału Konstytucyjnego stwierdzającego niezgodność z Konstytucją niektórych przepisów ustawy - Prawo lokalowe	1990-07-26	niepublikowana
		pos. nr 37	
90	w sprawie oceny niektórych aspektów działalności rządu Mieczysława Rakowskiego	1990-07-26	M.P. 1990 nr 30, poz. 231
		pos. nr 37	
91	o powołaniu Komisji Nadzwyczajnej w sprawie zbadania sprawy importu alkoholi	1990-07-26	niepublikowana
		pos. nr 37	
92	w sprawie przyjęcia do wiadomości oświadczenia Ministra Spraw Zagranicznych	1990-07-27	niepublikowana
		pos. nr 37	
93	w sprawie wykonania budżetu i innych planów finansowych w 1990 r. oraz absolutorium za 1989 r.	1990-07-27	M.P. 1990 nr 30, poz. 232
		pos. nr 37	
94	w sprawie sprawozdania z działalności NBP w 1989 r.	1990-07-27	M.P. 1990 nr 30, poz. 233
		pos. nr 37	
95	"o wyborze składów osobowych Komisji Polityki Przestrzennej, Budowlanej i Mieszkaniowej; Komisji Samorządu Terytorialnego; Komisji Przekształceń Własnościowych i Komisji Nadzwyczajnej do zbadania sprawy importu alkoholi"	1990-07-28	niepublikowana
		pos. nr 37	
96	w związku z proklamowaniem suwerenności państwowej Ukrainy i Białorusi	1990-07-28	M.P. 1990 nr 30, poz. 234
		pos. nr 37	
97	w sprawie powołania ministra przekształceń własnościowych	1990-09-14	M.P. 1990 nr 34, poz. 269
		pos. nr 38	
98	w sprawie powołania ministra rolnictwa i gospodarki żywnościowej	1990-09-14	M.P. 1990 nr 34, poz. 270
		pos. nr 38	
99	w sprawie odwołania ministra łączności	1990-09-14	M.P. 1990 nr 34, poz. 271
		pos. nr 38	
100	w sprawie powołania ministra łączności	1990-09-14	M.P. 1990 nr 34, poz. 272
		pos. nr 38	
101	w sprawie ratyfikacji Konwencji o Prawach Dziecka przyjętej przez Zgromadzenie Ogólne Narodów Zjednoczonych dnia 20 listopada 1989 r.	1990-09-21	niepublikowana
		pos. nr 39	
102	o skróceniu X Kadencji Sejmu oraz kadencji Senatu i Prezydenta Rzeczypospolitej Polskiej	1990-09-21	M.P. 1990 nr 36, poz. 289
		pos. nr 39	
103	w sprawie przebiegu realizacji programu stabilizacyjnego gospodarki oraz budżetu państwa	1990-10-11	M.P. 1990 nr 39, poz. 305
		pos. nr 41	
104	w sprawie realizacji budżetu państwa w pierwszym półroczu 1990 r.	1990-10-11	niepublikowana
		pos. nr 41	
105	w sprawie ochrony nazwy państwa	1990-10-12	M.P. 1990 nr 39, poz. 306
		pos. nr 41	
106	w sprawie odwołania ministra - członka Rady Ministrów A. Halla	1990-10-12	M.P. 1990 nr 39, poz. 307
		pos. nr 41	
107	w sprawie podstawowych kierunków prywatyzacji w roku 1990	1990-11-09	M.P. 1990 nr 43, poz. 333
		pos. nr 43	
108	w sprawie założeń do polityki energetycznej Polski do 2010 roku	1990-11-09	M.P. 1990 nr 43, poz. 332
		pos. nr 43	
109	w sprawie odwołania Rady Ministrów	1990-12-14	M.P. 1990 nr 48, poz. 364
		pos. nr 46	
110	w sprawie rządowych założeń polityki rolnej	1990-12-19	M.P. 1991 nr 1, poz. 1
		pos. nr 47	
111	w sprawie rządowego projektu ustawy o opodatkowaniu wzrostu wynagrodzeń w 1991 roku	1990-12-22	M.P. 1991 nr 1, poz. 2
		pos. nr 47	
112	w sprawie powołania Prezesa Rady Ministrów	1991-01-04	M.P. 1991 nr 1, poz. 3
		pos. nr 48	
113	w sprawie powołania Rady Ministrów	1991-01-12	M.P. 1991 nr 3, poz. 12
		pos. nr 49	
114	w sprawie kombatantów	1991-01-24	M.P. 1991 nr 5, poz. 27
		pos. nr 50	
115	w sprawie odwołania prezesa Narodowego Banku Polskiego	1991-01-25	M.P. 1991 nr 5, poz. 25
		pos. nr 50	
116	w sprawie powołania prezesa Narodowego Banku Polskiego	1991-01-25	M.P. 1991 nr 5, poz. 26
		pos. nr 50	
117	w sprawie powołania Komisji Nadzwyczajnej do rozpatrzenia projektów ustaw dotyczących spółdzielczości	1991-01-25	niepublikowana
		pos. nr 50	
118	w sprawie przeprowadzenia ogólnokrajowej konsultacji społecznej	1991-01-25	M.P. 1991 nr 5, poz. 28
		pos. nr 50	
119	w sprawie powołania Komisji Nadzwyczajnej do rozpatrzenia projektu ustawy o ochronie prawnej dziecka poczętego	1991-01-25	niepublikowana
		pos. nr 50	
120	w sprawie odwołania prezesa Najwyższej Izby Kontroli	1991-02-15	M.P. 1991 nr 7, poz. 45
		pos. nr 51	
121	w sprawie powołania prezesa Najwyższej Izby Kontroli	1991-02-15	M.P. 1991 nr 19, poz. 127
		pos. nr 51	
122	w sprawie realizacji ustawy budżetowej na 1991 r.	1991-02-23	M.P. 1991 nr 13, poz. 84
		pos. nr 52	
123	w sprawie założeń polityki gospodarczo-społecznej na rok 1991	1991-02-23	M.P. 1991 nr 13, poz. 83
		pos. nr 52	
124	w sprawie podstawowych kierunków prywatyzacji w 1991 r.	1991-02-23	M.P. 1991 nr 13, poz. 86
		pos. nr 52	
125	w sprawie założeń polityki pieniężnej na 1991 r.	1991-02-23	M.P. 1991 nr 13, poz. 85
		pos. nr 52	
126	w sprawie terminu rozwiązania się Sejmu	1991-03-09	M.P. 1991 nr 9, poz. 63
		pos. nr 53	
127	w sprawie terminu przedstawienia przez Komisję Konstytucyjną sprawozdania o projektach ustaw: Ordynacja wyborcza do Sejmu	1991-03-22	niepublikowana
		pos. nr 54	
128	w sprawie powołania Przewodniczącego Komitetu Badań Naukowych	1991-03-22	M.P. 1991 nr 13, poz. 87
		pos. nr 54	
129	w sprawie określenia trybu zgłaszania kandydatów na stanowisko Prezesa Najwyższej Izby Kontroli	1991-03-22	niepublikowana
		pos. nr 54	
130	w sprawie tragedii narodu kurdyjskiego	1991-04-05	niepublikowana
		pos. nr 55	
131	zobowiązująca rząd do złożenia projektu ustawy o powszechnym zaopatrzeniu emerytalnym pracowników i ich rodzin w terminie do 30 kwietnia br.	1991-04-05	niepublikowana
		pos. nr 55	
132	w sprawie polityki mieszkaniowej państwa	1991-04-20	niepublikowana
		pos. nr 55	
133	w sprawie polityki ekologicznej	1991-05-10	M.P. 1991 nr 18, poz. 118
		pos. nr 59	
134	w sprawie realizacji ustawy o ochronie i kształtowaniu środowiska	1991-05-10	M.P. 1991 nr 18, poz. 119
		pos. nr 59	
135	w sprawie zobowiązania Rządu do przedstawienia programu zmian w państwowym sektorze rolnictwa	1991-05-10	niepublikowana
		pos. nr 59	
136	w sprawie postępowania z dokumentami dotyczącymi ochrony prawnej dziecka poczętego	1991-05-17	M.P. 1991 nr 18, poz. 120
		pos. nr 60	
137	w sprawie powołania Komisji Nadzwyczajnej do rozpatrzenia niektórych inicjatyw ustawodawczychu 	1991-06-07	niepublikowana
	Przebieg procesu legislacyjnegodruk nr 902	pos. nr 62	
138	w sprawie załołżeń polityki mieszkaniowej państwa 	1991-06-15	M.P. 1991 nr 21, poz. 145
	Przebieg procesu legislacyjnegodruk nr 914	pos. nr 63	
139	w sprawie wprowadzenia podatku dochodowego od osób fizycznych 	1991-07-05	niepublikowana
	Przebieg procesu legislacyjnegodruk nr 948	pos. nr 66	
140	w sprawie sytuacji w radiu i telewizji 	1991-07-19	M.P. 1991 nr 24, poz. 165
	Przebieg procesu legislacyjnegodruk nr 981	pos. nr 68	
141	w sprawie Traktatu o stosunkach handlowych i gospodarczych między Rzecząpospolitą Polską a Stanami Zjednoczonymi Ameryki 	1991-07-26	M.P. 1991 nr 27, poz. 191
	Przebieg procesu legislacyjnegodruk nr 980	pos. nr 69	
142	w sprawie środków na zapobieganie i leczenie uzależnienia i chorób będących następstwem palenia tytoniu 	1991-07-26	M.P. 1991 nr 26, poz. 177
	Przebieg procesu legislacyjnegodruk nr 966	pos. nr 69	
143	w sprawie aktualnej sytuacji gospodarczej kraju oraz kierunków polityki gospodarczej w 1991 r. 	1991-07-27	M.P. 1991 nr 26, poz. 178
	Przebieg procesu legislacyjnegodruk nr 1009	pos. nr 69	
144	w sprawie przyjęcia sprawozdań z wykonania budżetu państwa za okres od 1 stycznia do 31 grudnia 1990 r., z gospodarki środkami Centralnego Funduszu Rozwoju Nauki i Techniki w 1990 r., z wykonania Funduszu Rozwoju Kultury za 1990 r., oraz w sprawie udzielenia Rządowi absolutorium 	1991-08-22	M.P. 1991 nr 28, poz. 199
	Przebieg procesu legislacyjnegodruk nr 1015	pos. nr 70	
145	w sprawie realizacji uchwał sejmowych dotyczących rolnictwa i gospodarki żywnościowej 	1991-08-24	M.P. 1991 nr 28, poz. 200
	Przebieg procesu legislacyjnegodruk nr 1037	pos. nr 70	
146	w sprawie trybu rozpatrzenia projektów ustaw związanych ze zmianą budżetu na 1991 r. 	1991-08-31	niepublikowana
	Przebieg procesu legislacyjnegodruk nr 1051	pos. nr 71	
147	w sprawie niepodległości Ukrainy 	1991-08-31	M.P. 1991 nr 29, poz. 205
	Przebieg procesu legislacyjnegodruk nr 1055	pos. nr 71	
148	w sprawie ogłoszenia niepodległości Białorusi 	1991-08-31	M.P. 1991 nr 29, poz. 206
	Przebieg procesu legislacyjnegodruk nr 1056	pos. nr 71	
149	w sprawie odwołania ministra przemysłu	1991-08-31	M.P. 1991 nr 29, poz. 207
		pos. nr 71	
150	w sprawie powołania ministra przemysłu i handlu	1991-08-31	M.P. 1991 nr 29, poz. 209
		pos. nr 71	
151	w sprawie odwołania prezesa Narodowego Banku Polskiego	1991-08-31	M.P. 1991 nr 29, poz. 208
		pos. nr 71	
152	w sprawie zmiany zasad odpłatności za leki i artykuły sanitarne 	1991-09-07	M.P. 1991 nr 34, poz. 250
	Przebieg procesu legislacyjnegodruk nr 968	pos. nr 72	
153	w sprawie powołania Komisji Nadzwyczajnej 	1991-09-07	niepublikowana
	Przebieg procesu legislacyjnegodruk nr 1070	pos. nr 72	
154	w sprawie powszechnego wprowadzenia indywidualnego rozliczenia ilości dostarczonej energii cieplnej 	1991-09-14	M.P. 1991 nr 30, poz. 218
	Przebieg procesu legislacyjnegodruk nr 1024	pos. nr 73	
155	w sprawie przyjęcia sprawozdania z działalności NBP w 1990 r. oraz z realizacji polityki pieniężnej w 1990r. 	1991-09-26	M.P. 1991 nr 33, poz. 236
	Przebieg procesu legislacyjnegodruk nr 1020	pos. nr 75	
	Przebieg procesu legislacyjnegodruk nr 1020-A		
156	w sprawie wykonywania przez Rząd postanowień uchwały Sejmu z dnia 24 sierpnia 1991 r. w sprawie realizacji uchwał sejmowych dotyczących rolnictwa i gospodarki żywnościowej 	1991-10-03	M.P. 1991 nr 33, poz. 237
	Przebieg procesu legislacyjnegodruk nr 1128	pos. nr 76	
157	w sprawie zobowiązania Rządu do realizacji uchwały z dnia 15 czerwca 1991 r. w zakresie finansowania i kredytowania budownictwa mieszkaniowego 	1991-10-04	M.P. 1991 nr 33, poz. 238
	Przebieg procesu legislacyjnegodruk nr 1136	pos. nr 76	
158	w sprawie wniosków sformułowanych przez Komisję Nadzwyczajną do Zbadania Działalności MSW 	1991-10-05	M.P. 1991 nr 33, poz. 239
	Przebieg procesu legislacyjnegodruk nr 1145	pos. nr 76	
159	w sprawie zmian w zakresie struktur i funkcjonowania przemysłu obronnego 	1991-10-09	niepublikowana
	Przebieg procesu legislacyjnegodruk nr 1127	pos. nr 77	
160	w sprawie zobowiązania Rządu do złożenia w Sejmie projektu nowej ustawy karnej skarbowej 	1991-10-10	M.P. 1991 nr 36, poz. 260
	Przebieg procesu legislacyjnegodruk nr 1160	pos. nr 77	
161	w sprawie zasad dzierżawy nieruchomości rolnych będących własnością Skarbu Państwa 	1991-10-11	niepublikowana
	Przebieg procesu legislacyjnegodruk nr 1142	pos. nr 77	
162	w sprawie ratyfikacji Traktatu między Rzecząpospolitą Polską a Republiką Federalną Niemiec o potwierdzeniu istniejącej między nimi granicy, podpisanego w Warszawie 14 listopada 1990 r. 	1991-10-18	M.P. 1991 nr 36, poz. 261
	Przebieg procesu legislacyjnegodruk nr 1188	pos. nr 78	
163	w sprawie traktatów zawartych między Rzecząpospolitą Polską a Republiką Federalną Niemiec o dobrym sąsiedztwie i przyjaznej współpracy, podpisanego w Bonn dnia 17 czerwca 1991 r. oraz o potwierdzeniu istniejącej między nimi granicy, podpisanego w Warszawie 14 listopada 1990 r. 	1991-10-18	M.P. 1991 nr 36, poz. 262
	Przebieg procesu legislacyjnegodruk nr 1188-a	pos. nr 78	
164	w sprawie realizacji podstawowych kierunków prywatyzacji w 1991 r. 	1991-10-19	M.P. 1991 nr 36, poz. 263
	Przebieg procesu legislacyjnegodruk nr 1150	pos. nr 78