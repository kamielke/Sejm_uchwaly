﻿#
# Wstepne przygotowanie danych do projektu SEJMUCH
#

#tablice na dane surowe
create table uchwaly_tst_X(
idrek int primary key auto_increment,
poz varchar(300),
tyt varchar(400),
c_data varchar(250), #na wszelki wypadek character
inne varchar(250),
druk varchar(80));

create table uchwaly_tst_I like  uchwaly_tst_X;
create table uchwaly_tst_II like  uchwaly_tst_X;
create table uchwaly_tst_III like  uchwaly_tst_X;
create table uchwaly_tst_IV like  uchwaly_tst_X;
create table uchwaly_tst_V like  uchwaly_tst_X;
create table uchwaly_tst_VI like  uchwaly_tst_X;
create table uchwaly_tst_VII like  uchwaly_tst_X;
create table uchwaly_tst_VIII like  uchwaly_tst_X;

#ladowanie X
LOAD DATA  INFILE 'C:/sejm/Uchwaly_X_uc.txt '
INTO TABLE uchwaly_tst_X
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(@lp,@tytul,@cdata,@inne)
set
poz=Trim(@lp),
tyt=Substr(Trim(@tytul),1,400),
c_data=Trim(@cdata),
inne=Trim(@inne);

#ladowanie I
LOAD DATA  INFILE 'C:/sejm/Uchwaly_I_uc.txt '
INTO TABLE uchwaly_tst_I
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(@lp,@tytul,@cdata,@inne)
set
poz=Trim(@lp),
tyt=Substr(Trim(@tytul),1,400),
c_data=Trim(@cdata),
inne=Trim(@inne);

#ladowanie II
LOAD DATA  INFILE 'C:/sejm/Uchwaly_II_uc.txt '
INTO TABLE uchwaly_tst_II
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(@lp,@tytul,@cdata,@inne)
set
poz=Trim(@lp),
tyt=Substr(Trim(@tytul),1,400),
c_data=Trim(@cdata),
inne=Trim(@inne);

#ladowanie III - zmiana układu danych wejściowych
LOAD DATA  INFILE 'C:/sejm/Uchwaly_III_uc.txt '
INTO TABLE uchwaly_tst_III
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(@lp,@tytul,@druk,@cdata,@inne)
set
poz=Trim(@lp),
tyt=Substr(Trim(@tytul),1,400),
c_data=Trim(@cdata),
inne=Trim(@inne),
druk=Trim(@druk);

#ladowanie IV
LOAD DATA  INFILE 'C:/sejm/Uchwaly_IV_uc.txt '
INTO TABLE uchwaly_tst_IV
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(@lp,@tytul,@druk,@cdata,@inne)
set
poz=Trim(@lp),
tyt=Substr(Trim(@tytul),1,400),
c_data=Trim(@cdata),
inne=Trim(@inne),
druk=Trim(@druk);

#ladowanie V
LOAD DATA  INFILE 'C:/sejm/Uchwaly_V_uc.txt '
INTO TABLE uchwaly_tst_V
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(@lp,@tytul,@druk,@cdata,@inne)
set
poz=Trim(@lp),
tyt=Substr(Trim(@tytul),1,400),
c_data=Trim(@cdata),
inne=Trim(@inne),
druk=Trim(@druk);

#ladowanie VI - kolejna zmiana układu danych wejściowych
LOAD DATA  INFILE 'C:/sejm/Uchwaly_VI_uc.txt '
INTO TABLE uchwaly_tst_VI
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(@lp,@tytul,@cdata,@inne,@druk)
set
poz=Trim(@lp),
tyt=Substr(Trim(@tytul),1,400),
c_data=Trim(@cdata),
inne=Trim(@inne),
druk=Trim(@druk);

#ladowanie VII
LOAD DATA  INFILE 'C:/sejm/Uchwaly_VII_uc.txt '
INTO TABLE uchwaly_tst_VII
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(@lp,@tytul,@cdata,@inne,@druk)
set
poz=Trim(@lp),
tyt=Substr(Trim(@tytul),1,400),
c_data=Trim(@cdata),
inne=Trim(@inne),
druk=Trim(@druk);

#ladowanie VIII
LOAD DATA  INFILE 'C:/sejm/Uchwaly_VIII_uc.txt '
INTO TABLE uchwaly_tst_VIII
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(@lp,@tytul,@cdata,@inne,@druk)
set
poz=Trim(@lp),
tyt=Substr(Trim(@tytul),1,400),
c_data=Trim(@cdata),
inne=Trim(@inne),
druk=Trim(@druk);

#doprowadzanie danych do jednorodnej postaci
# wiersze parzyste i nieparzyste mają inną zawartość
# ogólnie - pod wierszem z tytułem jest wiersz lub wiele wierszy z numerami druku
# dla jednej kadencji tytuły jednej uchwały też trzeba skleic z kilku wierszy

#---------------- kadencja X ---------------------------------------------------------------------
#usuwanie nadmiarowego napisu
update uchwaly_tst_x
  set tyt=Replace(tyt,'Przebieg procesu legislacyjnegodruk nr ','');

#przenoszenie danych o druku sejmowym z kolumny tytułu
update uchwaly_tst_x , (SELECT t1.idrek id, (Select group_concat(tyt) from uchwaly_tst_x t where t.idrek>t1.idrek and t.idrek<t2.idrek) cdruk
                        from uchwaly_tst_x t1, uchwaly_tst_x t2
                        where t1.poz>'' and t2.poz>'' and
                        cast(t1.poz as unsigned)=cast(t2.poz as unsigned)-1 ) as z1
  set druk=z1.cdruk
where length(poz)>0 and idrek=z1.id;

#nie chwyta ostatniego rekordu dlatego poprawka na ostatni rekord
set @maxpoz_x=(Select max(Cast(poz as unsigned)) from uchwaly_tst_x where poz>'');

update uchwaly_tst_x , (SELECT t1.idrek id, (Select group_concat(tyt) from uchwaly_tst_x t where t.idrek>t1.idrek ) cdruk
                        from uchwaly_tst_x t1 where t1.poz=Cast(@maxpoz_x as char)) as z1
  set druk=z1.cdruk
where poz=Cast(@maxpoz_x as char) and idrek=z1.id;

#usuwanie nadmiarowych wierszy
delete from uchwaly_tst_x
where length(poz)=0;

#--------------------------- kadencja I ----------------------------------------------------------
update uchwaly_tst_I
  set tyt=Replace(tyt,'Przebieg procesu legislacyjnegodruk nr ','');

update uchwaly_tst_I
  set tyt=Replace(tyt,'Przebieg procesu legislacyjnego','');

update uchwaly_tst_I, (SELECT t1.idrek id, (Select group_concat(tyt) from uchwaly_tst_I t where t.idrek>t1.idrek and t.idrek<t2.idrek) cdruk
                      from uchwaly_tst_I t1, uchwaly_tst_I t2
                      where t1.poz>'' and t2.poz>'' and
                      cast(t1.poz as unsigned)=cast(t2.poz as unsigned)-1 ) as z1
  set druk=z1.cdruk
where length(poz)>0 and idrek=z1.id;

set @maxpoz_I=(Select max(Cast(poz as unsigned)) from uchwaly_tst_I where poz>'');

update uchwaly_tst_I , (SELECT t1.idrek id, (Select group_concat(tyt) from uchwaly_tst_I t where t.idrek>t1.idrek ) cdruk
                        from uchwaly_tst_I t1 where t1.poz=Cast(@maxpoz_I as char)) as z1
  set druk=z1.cdruk
where poz=Cast(@maxpoz_I as char) and idrek=z1.id;

delete from uchwaly_tst_I
where length(poz)=0;

#--------------------------------- kadencja  II ---------------------------------
update uchwaly_tst_II
  set tyt=Replace(tyt,'Przebieg procesu legislacyjnegodruk nr ','');

update uchwaly_tst_II
  set tyt=Replace(tyt,'Przebieg procesu legislacyjnego druk nr ','');

update uchwaly_tst_II , (SELECT t1.idrek id, (Select group_concat(tyt) from uchwaly_tst_II t where t.idrek>t1.idrek and t.idrek<t2.idrek) cdruk
                         from uchwaly_tst_II t1, uchwaly_tst_II t2
                         where t1.poz>'' and t2.poz>'' and
                        cast(t1.poz as unsigned)=cast(t2.poz as unsigned)-1 ) as z1
set druk=z1.cdruk
where length(poz)>0 and idrek=z1.id;

set @maxpoz_II=(Select max(Cast(poz as unsigned)) from uchwaly_tst_II where poz>'');

update uchwaly_tst_II , (SELECT t1.idrek id, (Select group_concat(tyt) from uchwaly_tst_II t where t.idrek>t1.idrek ) cdruk
                        from uchwaly_tst_II t1 where t1.poz=Cast(@maxpoz_II as char)) as z1
  set druk=z1.cdruk
where poz=Cast(@maxpoz_II as char) and idrek=z1.id;

delete from uchwaly_tst_II
where length(poz)=0;

#--------------------------------- kadencja III ----------------------------------------
update uchwaly_tst_III
  set druk=Replace(druk,'Przebieg procesu legislacyjnego',''),
      inne=Replace(inne,'Tekst uchwały','');

#usuwanie daty zgłoszenia z kolumny druk
update uchwaly_tst_III
  set druk=''
where druk regexp '^[0-9]{2}-[0-9]{2}-[0-9]{4}';

#przenoszenie inf o monitorze z kolejnego wiersza do wiersza z numerem pozycji
update uchwaly_tst_III z, (Select idrek id, inne from uchwaly_tst_III where length(poz)=0) as t
  set z.inne=t.inne
where length(poz)>0 and idrek=t.id-1;

#konkatenecja danych z kolumny druk w ramach uchwały,z pominięciem przecinków
update uchwaly_tst_III z, (SELECT t1.idrek id, (Select group_concat(druk separator '') from uchwaly_tst_III t where t.idrek>=t1.idrek and t.idrek<t2.idrek) cdruk
                           from uchwaly_tst_III t1, uchwaly_tst_III t2
                           where t1.poz>'' and t2.poz>'' and
                          cast(t1.poz as unsigned)=cast(t2.poz as unsigned)-1 ) as z1
  set z.druk=cdruk
where z.idrek = z1.id;

set @maxpoz_III=(Select max(Cast(poz as unsigned)) from uchwaly_tst_III where poz>'');

update uchwaly_tst_III , (SELECT t1.idrek id, (Select group_concat(druk separator ' ') from uchwaly_tst_III t where t.idrek>=t1.idrek ) cdruk
                          from uchwaly_tst_III t1 where t1.poz=Cast(@maxpoz_III as char)) as z1
  set druk=cdruk
where poz=Cast(@maxpoz_III as char) and idrek=z1.id;

delete from uchwaly_tst_III
where length(poz)=0 ;

#------------------------------------------ kadencja IV ---------------------------------
update uchwaly_tst_IV
  set druk=Replace(druk,'Przebieg procesu legislacyjnego',''),
      inne=Replace(inne,'Tekst uchwały','');

update uchwaly_tst_iV
  set druk=''
where druk regexp '^[0-9]{2}-[0-9]{2}-[0-9]{4}';


update uchwaly_tst_IV z, (Select idrek id, inne from uchwaly_tst_IV where length(poz)=0) as t
  set z.inne=t.inne
where length(poz)>0 and idrek=t.id-1;

update uchwaly_tst_IV z,(SELECT t1.idrek id, (Select group_concat(druk separator ' ') from uchwaly_tst_iv t where t.idrek>=t1.idrek and t.idrek<t2.idrek) cdruk
                      from uchwaly_tst_iv t1, uchwaly_tst_iv t2
                      where t1.poz>'' and t2.poz>'' and
                      cast(t1.poz as unsigned)=cast(t2.poz as unsigned)-1 ) as z1
  set z.druk=cdruk
where z.idrek = z1.id;

set @maxpoz_IV=(Select max(Cast(poz as unsigned)) from uchwaly_tst_IV where poz>'');

update uchwaly_tst_IV , (SELECT t1.idrek id, (Select group_concat(druk separator ' ') from uchwaly_tst_IV t where t.idrek>=t1.idrek ) cdruk
                        from uchwaly_tst_IV t1 where t1.poz=Cast(@maxpoz_IV as char)) as z1
  set druk=cdruk
where poz=Cast(@maxpoz_IV as char) and idrek=z1.id;

delete from uchwaly_tst_IV
where length(poz)=0 ;

update uchwaly_tst_V
  set druk=Replace(druk,'Przebieg procesu legislacyjnego',''),
      inne=Replace(inne,'Tekst uchwały','');

#------------------------------------- kadencja V--------------------------------------------
# usunięcie nadmiarowej daty z kolumny z drukiem
update uchwaly_tst_V
  set druk=''
where druk regexp '^[0-9]{2}-[0-9]{2}-[0-9]{4}';

update uchwaly_tst_V z, (Select idrek id, inne from uchwaly_tst_V where length(poz)=0) as t
  set z.inne=t.inne
where length(poz)>0 and z.idrek=t.id-1;

update uchwaly_tst_V z, (SELECT t1.idrek id, (Select group_concat(druk separator ' ') from uchwaly_tst_v t where t.idrek>=t1.idrek and t.idrek<t2.idrek) cdruk
                          from uchwaly_tst_v t1, uchwaly_tst_v t2
                          where t1.poz>'' and t2.poz>'' and
                          cast(t1.poz as unsigned)=cast(t2.poz as unsigned)-1 ) as z1
  set z.druk=cdruk
where z.idrek = z1.id;

#polączenie tytulów
update uchwaly_tst_V z, (SELECT t1.idrek id, (Select group_concat(Trim(tyt) separator ' ') from uchwaly_tst_v t where t.idrek>=t1.idrek and t.idrek<t2.idrek) ctyt
                         from uchwaly_tst_v t1, uchwaly_tst_v t2
                         where t1.poz>'' and t2.poz>'' and
                          cast(t1.poz as unsigned)=cast(t2.poz as unsigned)-1 ) as z1
  set z.tyt=Substr(ctyt,1,400)
where z.idrek = z1.id;

set @maxpoz_V=(Select max(Cast(poz as unsigned)) from uchwaly_tst_V where poz>'');

update uchwaly_tst_V , (SELECT t1.idrek id, (Select group_concat(druk separator ' ') from uchwaly_tst_V t where t.idrek>=t1.idrek ) cdruk
                        from uchwaly_tst_V t1 where t1.poz=Cast(@maxpoz_V as char)) as z1
  set druk=cdruk
where poz=Cast(@maxpoz_V as char) and idrek=z1.id;

delete from uchwaly_tst_V
where length(poz)=0 ;

#------------------------------ kadencja VI -----------------------------------------------------------
update uchwaly_tst_VI
  set druk=Replace(druk,'Przebieg procesu legislacyjnego','');

update uchwaly_tst_VI z, (Select idrek id, inne from uchwaly_tst_VI where length(poz)=0) as t
  set z.inne=t.inne
where length(poz)>0 and z.idrek=t.id-1;

delete from uchwaly_tst_VI
where length(poz)=0 ;

#------------------------------ kadencja VII ---------------------------------------------------------

update uchwaly_tst_VII z, (Select idrek id, inne from uchwaly_tst_VII where length(poz)=0) as t
  set z.inne=t.inne
where length(poz)>0 and z.idrek=t.id-1;

delete from uchwaly_tst_VII
where length(poz)=0 ;

#----------------------------- kadencja VIII --------------------------------------------------------
update uchwaly_tst_VIII
  set inne=''
where inne like 'pdf%';

update uchwaly_tst_VIII z, (Select idrek id, inne from uchwaly_tst_VIII where length(poz)=0) as t
  set z.inne=t.inne
where length(poz)>0 and z.idrek=t.id-1;

delete from uchwaly_tst_VIII
where length(poz)=0 ;

#----- Scalanie danych w jeden zbiór w  układzie chronologicznym ----------------------------------
CREATE TABLE uchwaly(
idrek int primary key auto_increment,
kadencja varchar(4),
ord_kadencja int,
poz varchar(4),
tytul varchar(400),
info varchar(35),
druk varchar(80),
data_uchw date,
dzien_kad int,
ind_O varchar(40) default '',
ind_U varchar(40) default '',
ind_M varchar(20) default '',
typ char(3) default '');

#robienie jednej tablicy wynikowej
Insert into uchwaly (kadencja,ord_kadencja,poz,tytul,info,druk,data_uchw)
Select 'X',1,poz,tyt,inne,druk, Str_to_Date(c_data,'%Y-%m-%d') from uchwaly_tst_X union
Select 'I',2,poz,tyt,inne,druk, Str_to_Date(c_data,'%Y-%m-%d') from uchwaly_tst_I union
Select 'II',3,poz,tyt,inne,druk,Str_to_Date(c_data,'%Y-%m-%d') from uchwaly_tst_II union
Select 'III',4,poz,tyt,inne,druk, Str_to_Date(c_data,'%d-%m-%Y') from uchwaly_tst_III union
Select 'IV',5,poz,tyt,inne,druk, Str_to_Date(c_data,'%d-%m-%Y')from uchwaly_tst_IV union
Select 'V',6,poz,tyt,inne,druk, Str_to_Date(c_data,'%d-%m-%Y') from uchwaly_tst_V ;

#cd. nie da rady w jednym union zmienić kolejność rekordów tj. dodać inny order by, a lepiej się oglada chronologicznie
Insert into uchwaly (kadencja,ord_kadencja,poz,tytul,info,druk,data_uchw)
Select 'VI',7,poz,tyt,inne,druk,Str_to_Date(c_data,'%d-%m-%Y') from uchwaly_tst_VI u order by u.idrek desc;

Insert into uchwaly (kadencja,ord_kadencja,poz,tytul,info,druk,data_uchw)
Select 'VII',8,poz,tyt,inne,druk,Str_to_Date(c_data,'%d-%m-%Y') from uchwaly_tst_VII u order by u.idrek desc;

Insert into uchwaly (kadencja,ord_kadencja,poz,tytul,info,druk,data_uchw)
Select 'VIII',9,poz,tyt,inne,druk,Str_to_Date(c_data,'%d-%m-%Y') from uchwaly_tst_VIII u order by u.idrek desc;

# --------- doczyszczanie ---------------
#początkowe i końcowe cudzysłowy tam gdzie są
Update uchwaly
  set tytul=Substr(tytul,2,length(Ltrim(tytul))-3)
where tytul regexp '^"';

#podwójne uszy "" na jedne "
Update uchwaly
  set tytul=replace(tytul,'""','"')
where tytul regexp '""';

#ustawienie umownego dnia kadencji
Update uchwaly u, (Select kadencja,min(data_uchw) mdata from uchwaly group by 1) as z
  set
      dzien_kad=DateDiff(data_uchw,mdata)+1
where u.kadencja=z.kadencja;

#---------------------- wzorce klasyfikacyjne -------------------------
create table klucze(
idkl int unique not null,
klucz varchar(200) not null,
typ char(1) not null);

Create table uchwaly_mix like uchwaly;


#ładowanie kluczy
LOAD DATA  INFILE 'C:/sejm1/sejm_wzorce2.txt '
INTO TABLE klucze
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(idkl,klucz,typ);

#dodanie znaczników typu O
Update uchwaly z,
       (Select t.idrek,Group_concat(k.idkl order by k.idkl) ind_O from uchwaly t, klucze k where k.typ='O' and tytul rlike binary k.klucz group by t.idrek) as z1
  set z.ind_O =z1.ind_O,
      z.typ='O'
where z.idrek=z1.idrek;

Update uchwaly z,
       (Select t.idrek,Group_concat(k.idkl order by k.idkl) ind_M from uchwaly t, klucze k where k.typ='M' and tytul rlike binary k.klucz group by t.idrek) as z1
  set z.ind_M =z1.ind_M,
      z.typ=Concat(z.typ,'M')
where z.idrek=z1.idrek;

#dodanie znaczników typu U
Update uchwaly z,
       (Select t.idrek,Group_concat(k.idkl order by k.idkl) ind_U from uchwaly t, klucze k where k.typ='U' and tytul rlike binary k.klucz group by t.idrek) as z2
  set z.ind_U=z2.ind_U,
      z.typ=Concat(z.typ,'U')
where z.idrek=z2.idrek;

Insert into uchwaly_mix
  Select * from uchwaly where length(typ) in (0,2);

# ------------------- korekta typu dla danych niejednoznacznych regułami decyzyjnymi -----------------

update uchwaly
  set typ=if( ind_O rlike '1' ,'O','M')
where typ ='OM' ;

# klucz 17,16 wyznacza U
update uchwaly
  set typ= if( (ind_U rlike '17') or (ind_U rlike '16'),'U','O')
where typ='OU';

#klucz 12,13 motywacyh=jne
update uchwaly
  set typ=if( (ind_M rlike '12') or (ind_M rlike '13'),'M','U')
where typ ='MU' ;

update uchwaly
  set typ='M'
where typ='' ;

# te dane bedą eksportowane
Create view v_uchwaly as
Select idrek,kadencja,ord_kadencja ord_kad,tytul,info,druk,data_uchw,dzien_kad,typ,ind_O,ind_U,ind_M
from uchwaly order by idrek;

#troche info ile czego jest
Create view v_statu as
Select kadencja,sum(if(typ='O',1,0)) as ileO,sum(if(typ='M',1,0)) as ileM, sum(if(typ='U',1,0)) as ileU from uchwaly
group by 1 order by ord_kadencja;

#wykorzystanie jakie są indeksy
Create view v_ind_krot as
Select 'ind_O' typ_wzorca,typ typ_uchwaly,ind_O wzorce, count(*) from uchwaly group by 1,2,3 union
Select 'ind_M',typ,ind_M, count(*) from uchwaly group by 1,2,3 union
Select 'ind_U',typ,ind_U, count(*) from uchwaly group by 1,2,3  ;

#ile trafień miały klucze w jakiej kategorii
Create view v_klucz_krot as
Select klucz,u.typ typ_uchawly,count(*) krotnosc from  uchwaly u, klucze where tytul rlike binary klucz group by 1,2;