﻿idrek	klucz	typ
1	(R|r)ocznic|leci(e|a)	O
2	urodz	O
3	śmierci|zamordowania	O
4	upamiętni|uczczeni|uhonorowani|wyrażenia wdzięczności|hołd|honor|poświęcona pamięci	O
5	tragicz|mordu|prześladowa|ludobój|(T|t)raged|agresj|(Z|z)brodni|Powstania|((b|B)itwy)|((W|w)ojny)|(Z|z)rywu|terror|Armii|Walki|żołnierz|Zbrojnych'	O
6	(ustanowieni|ogłoszeni|Międzynarodowego)(.)*(D|d)(nia|niem)	O
7	roku[0-9 ]*(R|r)okiem	O
8	z okazji (Święta|otwarcia)	O
9	potępienia	O
10	solidarności z	O
11	nadania (.)*imienia	O
12	^(R|r)ezolucja|(D|d)eklaracja	M
13	^(A|a)pel|^Posłanie	M
14	sprawie(.)*((z|Z)ałożeń|planu|polityki)	M
15	kierunków prywatyzacji|zobowiązująca (R|r)ząd	M
16	sprawie (utworzenia|ustalenia|ważności|trybu|terminu|usunięcia niezgodności)	U
17	sprawie(.)*(wyboru|powołania|odwołania|zwolnienia|uzupełnienia|obsadzenia|ustąpienia)	U
18	(T|t)raktat|ratyfikac	U
19	określenia (trybu|liczby|zasad)	U
20	(wykonania|realizacji)(.)*(ustaw|uchwał)	U
21	(prawozdania|oświadczenia|nformacji|wystapienia)(.)*(ządu|Rady|inistr|arszałk|zecznika|remiera|rezesa|NBP|Banku|(K|k)omis)	U
22	wyboru|wyborczych|wyborów|kandydat|członk|posła|poseł|kadenc|wygaśnięcia mandatu|(zmian|zmiany)( w skład| sekretarz)	U
23	budżet|bilans|podatek|podatk|finansowani|administrac|obronnoś	U
24	(k|K)omisji (Konstytuc|Nadzwycz|(Ś|ś)led|Sejm|sejmo|do|Ustaw)	U
25	(odpowiedzialności|pociągnięci)(.)*(karnej|karno|konstyt|cywil)	U
26	immunitet	U
27	(e|E)tyki (p|P)osel	U
28	eferendum	U
29	egulamin(.)*Sejm	U
30	wotum (nieufności|zaufania)	U
31	Izb(.)*Kontroli	U
32	Senatu|Sądu|Rady Ministrów	U
33	(Parlamentu|Unii|Unią|Rady|Kolegium) Europ	U
34	orzecze(.)*Trybunału	U
35	Rzecznik(.)*Praw Obywatelskich	U
36	Skarb(.)*Państwa	U
37	Krajowej Rady Radiofonii i Telewizji	U